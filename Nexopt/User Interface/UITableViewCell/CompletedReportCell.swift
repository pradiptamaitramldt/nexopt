//
//  CompletedReportCell.swift
//  Nexopt
//
//  Created by Pradipta on 06/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

protocol SendReportDelegate {
    func sendReport(_ sender : UIButton)
}

class CompletedReportCell: UITableViewCell {

    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var assignID: UILabel!
    @IBOutlet weak var buttonSeeReport: UIButton!
    @IBOutlet weak var assignFron: UILabel!
    @IBOutlet weak var assignTo: UILabel!
    @IBOutlet weak var assignTime: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
    
    var delegate : SendReportDelegate?
    
    private let imgSelected = UIImage(named: "Selected_Driving_icon")
    private let imgNotSelected = UIImage(named: "unselected-icon_")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkIcon.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.checkIcon.image = selected ? imgSelected : imgNotSelected
    }
    
    @IBAction func buttonSeeReportAction(_ sender: Any) {
        self.delegate?.sendReport(sender as! UIButton)
    }
    
}
