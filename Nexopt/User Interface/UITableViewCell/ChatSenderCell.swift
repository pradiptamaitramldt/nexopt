//
//  ChatSenderCell.swift
//  Nexopt
//
//  Created by Pradipta on 07/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class ChatSenderCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textViewMessage: UITextView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
