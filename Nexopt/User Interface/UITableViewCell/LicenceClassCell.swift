//
//  LicenceClassCell.swift
//  Nexopt
//
//  Created by Pradipta on 08/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LicenceClassCell: UITableViewCell {

    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var labelClass: UILabel!
    
    private let imgSelected = UIImage(named: "Selected_Driving_icon")
    private let imgNotSelected = UIImage(named: "unselected-icon_")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkIcon.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.checkIcon.image = selected ? imgSelected : imgNotSelected
    }

}
