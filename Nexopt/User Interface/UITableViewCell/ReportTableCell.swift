//
//  ReportTableCell.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

protocol EditReportDelegate {
    func editReport(_ sender : UIButton)
}

class ReportTableCell: UITableViewCell {
    @IBOutlet weak var assignID: UILabel!
    @IBOutlet weak var assignFrom: UILabel!
    @IBOutlet weak var assignTo: UILabel!
    @IBOutlet weak var assignTime: UILabel!
    @IBOutlet weak var buttonEditReport: UIButton!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var checkIcon: UIImageView!
    
    var delegate : EditReportDelegate?
    
    private let imgSelected = UIImage(named: "Selected_Driving_icon")
    private let imgNotSelected = UIImage(named: "unselected-icon_")
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.checkIcon.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        self.checkIcon.image = selected ? imgSelected : imgNotSelected
    }
    
    @IBAction func buttonEditReportAction(_ sender: Any) {
        self.delegate?.editReport(sender as! UIButton)
    }
    
}
