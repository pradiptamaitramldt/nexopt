//
//  LeftMenu.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LeftMenu: UIViewController {

    @IBOutlet weak var profileIcon: UIImageView!
    @IBOutlet weak var table_view: UITableView!
    
    var menuNameArray : [String] = []
    var menuImageArray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        menuNameArray = ["Home", "Open Reports","Completed Reports", "My Profile", "My Messages", "Settings", "Logout"]
        menuImageArray = ["Home_icon", "Open-Reports_icon", "Completed-Reports_icon", "My-Profile_icon", "My-Messages_icon", "Settings_icon", "Logout_icon"]
        self.table_view.delegate=self
        self.table_view.dataSource=self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LeftMenu : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNameArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuTableCell", for: indexPath) as! LeftMenuTableCell
        cell.menuTitle.text = menuNameArray[indexPath.row]
        cell.menuIcon.image = UIImage(named: menuImageArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell:LeftMenuTableCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuTableCell
        selectedCell.contentView.backgroundColor = UIColor(red: 63.0/255, green: 77.0/255, blue: 124.0/255, alpha: 1.0)
        selectedCell.menuTitle.textColor=UIColor.white
        var selectedIcon = menuImageArray[indexPath.row]
        selectedIcon=selectedIcon+"_b"
        selectedCell.menuIcon.image=UIImage(named: selectedIcon)
        if indexPath.row==0 {
            let storyBoard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            vc.pageFrom = ""
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==1 {
            let storyBoard = UIStoryboard(name: "Reports", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "ReportsView") as! ReportsView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==2 {
            let storyBoard = UIStoryboard(name: "Reports", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "CompletedReportsView") as! CompletedReportsView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==3 {
            let storyBoard = UIStoryboard(name: "MyProfile", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyProfileView") as! MyProfileView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==4 {
            let storyBoard = UIStoryboard(name: "Messages", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "MyMessageView") as! MyMessageView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        } else if indexPath.row==5 {
            /*let vc = mainStoryBoard.instantiateViewController(withIdentifier: "PersonalAccountView") as! PersonalAccountView
            SlideNavigationController.sharedInstance().popAllAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)*/
        } else if indexPath.row==6 {
            /*let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
            let vc = mainStoryBoard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)*/
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:LeftMenuTableCell = tableView.cellForRow(at: indexPath as IndexPath)! as! LeftMenuTableCell
        selectedCell.contentView.backgroundColor = UIColor(red: 52.0/255, green: 63.0/255, blue: 95.0/255, alpha: 1.0)
        selectedCell.menuTitle.textColor=UIColor(red: 110.0/255, green: 120.0/255, blue: 153.0/255, alpha: 1.0)
        let selectedIcon = menuImageArray[indexPath.row]
        selectedCell.menuIcon.image=UIImage(named: selectedIcon)
    }
}
