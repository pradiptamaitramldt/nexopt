//
//  LoginView.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LoginView: UIViewController {

    @IBOutlet weak var textFieldUserName: UITextField!
    @IBOutlet weak var textFieldPassword: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var loginCommand: LoginCommand?
    var utility = Utility()
    var activeField   : UIView?
    var binders: Array<Any>?
    var isLaunched = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldUserName.text = "test_user@nexopt.com"
        self.textFieldPassword.text = "sp123"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(!self.isLaunched){
            
            let sessionRep = ETWSessionRepository.shared
            
            sessionRep.load()
            
            if let uid = sessionRep.uid, let password = sessionRep.password{
                
                self.textFieldUserName.text = uid
                self.textFieldPassword.text = password
                let login = LoginCommand(userID: uid, password: password)
                self.performLogin(loginCommand: login)
            }
            
            self.isLaunched = true
        }
    }
    
    // MARK: - Private
    private func setup() {
        self.loginCommand = LoginCommand.init(userID: nil, password: nil)
    }
    
    private func startLoading() {
        self.view.isUserInteractionEnabled = false;
        self.activityIndicator.startAnimating();
        self.activityIndicator.isHidden = false;
    }
    
    private func stopLoading() {
        self.activityIndicator.stopAnimating();
        self.view.isUserInteractionEnabled = true;
        self.activityIndicator.isHidden = true;
    }
    
    private func updateCommand() {
        self.loginCommand?.userID = self.textFieldUserName.text?.trimmingCharacters(in: .whitespaces)
        self.loginCommand?.password = self.textFieldPassword.text
    }
    
    func performLogin(loginCommand: LoginCommand?) {
        
        let loginRepository = LoginRepository()
        loginRepository.loginUser(loginCommand: loginCommand!, vc: self) { (response, success, error) in
            self.stopLoading()
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                if (response._id) != nil {
                    self.utility.Save(response._id!, keyname: Constants.Strings.OwnerID)
                }
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TermsConditionsView") as! TermsConditionsView
                SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func mapUserData(response: [String:Any]) {
        
    }
    
    @IBAction func buttonLoginAction(_ sender: Any) {
        /*let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TermsConditionsView") as! TermsConditionsView
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)*/
        self.updateCommand()
        self.loginCommand?.validate(completion: { (isValid, message) in
            
            // Start activity
            //self.startLoading()
            if(isValid){
                guard let loginCommand = self.loginCommand else{
                    return
                }
                self.performLogin(loginCommand: loginCommand)
            }else{
                self.stopLoading()
                Utility.showAlert(withMessage: message!, onController: self)
            }
        })
    }
    @IBAction func buttonResendLoginAction(_ sender: Any) {
    }
    
}
