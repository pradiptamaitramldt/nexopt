//
//  HomeView.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit
import Mapbox

class HomeView: UIViewController {

    @IBOutlet weak var mapView: MGLMapView!
    @IBOutlet weak var viewPopup: UIView!
    @IBOutlet weak var viewCarInfo: UIView!
    @IBOutlet weak var viewCarReport: UIView!
    @IBOutlet weak var labelDriverID: UILabel!
    @IBOutlet weak var labelCarName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelFuel: UILabel!
    @IBOutlet weak var labelMileage: UILabel!
    @IBOutlet weak var labelReportDate: UILabel!
    @IBOutlet weak var labelFromName: UILabel!
    @IBOutlet weak var labelFromAddress: UILabel!
    @IBOutlet weak var labelFromTime: UILabel!
    @IBOutlet weak var labelFromDistance: UILabel!
    @IBOutlet weak var labelToName: UILabel!
    @IBOutlet weak var labelToAddress: UILabel!
    @IBOutlet weak var labelToTime: UILabel!
    @IBOutlet weak var labelToDistance: UILabel!
    @IBOutlet weak var buttonScan: UIButton!
    @IBOutlet weak var buttonEndDrive: UIButton!
    @IBOutlet weak var buttonInfo: UIButton!
    @IBOutlet weak var buttonSendReport: UIButton!
    @IBOutlet weak var buttonLocation: UIButton!
    @IBOutlet weak var dieselCollectionView: UICollectionView!
    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var textFieldPartnerName: UITextField!
    @IBOutlet weak var textFieldCompanyName: UITextField!
    @IBOutlet weak var textFieldReasonForVisit: UITextField!
    @IBOutlet weak var textFieldNotes: UITextField!
    let locationManager = CLLocationManager()
    var lattitude = Double()
    var longitude = Double()
    var utility = Utility()
    var assetArray = FetchAssetsResponseModel()
    var pageFrom : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        if self.pageFrom == "EditReport" {
            self.viewCarReport.isHidden=false
            self.buttonInfo.isHidden=true
            self.buttonLocation.isHidden=true
            self.buttonSendReport.isHidden=false
        }else if self.pageFrom == "SendReport" {
            self.viewCarReport.isHidden=false
            self.buttonInfo.isHidden=true
            self.buttonLocation.isHidden=true
            self.buttonSendReport.isHidden=false
        } else {
            self.viewCarReport.isHidden=true
            self.buttonInfo.isHidden=false
            self.buttonLocation.isHidden=false
            self.buttonSendReport.isHidden=true
        }
        self.viewPopup.isHidden=true
        self.viewCarInfo.isHidden=true
        
        self.buttonScan.isHidden=true
        self.buttonEndDrive.isHidden=true
        self.dieselCollectionView.delegate=self
        self.dieselCollectionView.dataSource=self
        self.textFieldPartnerName.delegate=self
        self.textFieldCompanyName.delegate=self
        self.textFieldReasonForVisit.delegate=self
        self.fetchAssets()
        self.setMapView()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name(Constants.Strings.kQRScanSuccess), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    func fetchAssets() {
        let homeRepository = HomeRepository()
        let owner_id = utility.Retrive(Constants.Strings.OwnerID)
        homeRepository.fetchAssets(owner_id: owner_id as! String, vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                self.assetArray = response
                self.fetchAssetInfo(source_id: "\(String(describing: self.assetArray[0].sourceID!))")
                
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func fetchAssetInfo(source_id: String) {
        let homeRepository = HomeRepository()
        homeRepository.fetchAssetInfo(source_id: source_id, vc: self) { (response, success, error) in
            if (error != nil) {
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
            if (success) {
                let dateStamp = response.timestamp!
                let date = Utility.convertTimeStampToDate(timeStamp: "\(String(describing: dateStamp))")
                print("date...\(date)")
            }else{
                Utility.showAlert(withMessage: (error?.localizedDescription)!, onController: self)
            }
        }
    }
    
    func timeDifference(timestamp: String) -> Int {
        let car_date_str = Utility.convertTimeStampToDate(timeStamp: "\(timestamp)")
        let today_date_str = Utility.getlocalToUTCFormattedDate(date: Date(), format: "dd.MM.yyyy HH:mm:ss")
        let car_date = Utility.getFormmatedDateFrom(date: car_date_str, format: "dd.MM.yyyy HH:mm:ss")
        let today_date = Utility.getFormmatedDateFrom(date: today_date_str, format: "dd.MM.yyyy HH:mm:ss")
        let diff = Int(car_date.timeIntervalSince1970 - today_date.timeIntervalSince1970)
        let hours = diff / 3600
        let minutes = (diff - hours * 3600) / 60
        let total_minutes = (hours * 3600 + minutes)
        return total_minutes
    }
    
    func setMapView() {
        // Set the map’s center coordinate and zoom level.
        self.mapView.delegate = self
        self.mapView.showsUserLocation=true
        self.mapView.userTrackingMode = .follow
    }
    
    // This method is called everytime when a cover request accept/deny happens or request off accept deny happens or get shift happens or private mode add crew happens.
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.viewPopup.isHidden=true
        if let annotations = mapView.annotations {
            mapView.removeAnnotations(annotations)
        }
        self.viewCarInfo.isHidden=false
        self.buttonScan.isHidden=true
        self.buttonEndDrive.isHidden=false
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonInfoAction(_ sender: Any) {
    }
    @IBAction func buttonCurrentLocationAction(_ sender: Any) {
        self.setMapView()
    }
    @IBAction func buttonScanToDriveAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "QRCode", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRScannerDriveView") as! QRScannerDriveView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buttonEndDriveAction(_ sender: Any) {
    }
    @IBAction func buttonSendReportAction(_ sender: Any) {
    }
}
extension HomeView : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        self.lattitude = locValue.latitude
        self.longitude = locValue.longitude
    }
}
extension HomeView : MGLMapViewDelegate {
    func mapViewDidFinishLoadingMap(_ mapView: MGLMapView) {
        print("locations = \(self.lattitude) \(self.longitude)")
        var annotationArray : [MGLPointAnnotation] = []
        for item in self.assetArray {
            let time_difference = self.timeDifference(timestamp: "\(item.timestamp!)")
            let time_hour = time_difference / 60
            let time_minute = (time_difference - time_hour * 60)
            print("hour...\(time_hour)")
            print("minute...\(time_minute)")
            if let info = item.info {
                if let category = info.category {
                    var status : String = ""
                    if time_hour>0{
                        status = "offline"
                    }else if (time_hour==0 && time_minute>=2) {
                        status = "offline"
                    }else{
                        status = "online"
                    }
                    let hello = MGLPointAnnotation()
                    hello.coordinate = CLLocationCoordinate2D(latitude: (item.properties?.lat)!, longitude: (item.properties?.long)!)
                    hello.title = category
                    hello.subtitle = status
                    annotationArray.append(hello)
                }
            }
        }
        self.mapView.addAnnotations(annotationArray)
        /*let hello = MGLPointAnnotation()
        hello.coordinate = CLLocationCoordinate2D(latitude: self.lattitude+0.001, longitude: self.longitude)
        hello.title = self.assetArray[0].info?.category
        hello.subtitle = ""
        self.mapView.addAnnotation(hello)
        
        let hello2 = MGLPointAnnotation()
        hello2.coordinate = CLLocationCoordinate2D(latitude: self.lattitude+0.002, longitude: self.longitude+0.001 )
        hello2.title = "B"
        hello2.subtitle = ""
        self.mapView.addAnnotation(hello2)*/
    }
    
    // Use the default marker. See also: our view annotation or custom marker examples.
    
    // Allow callout view to appear when an annotation is tapped.
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return false
    }
    
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        // Substitute our custom view for the user location annotation. This custom view is defined below.
        if annotation is MGLUserLocation && mapView.userLocation != nil {
            return CustomMapAnnotation()
        }
        return nil
    }
    
    // Optional: tap the user location annotation to toggle heading tracking mode.
    /*private func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        if mapView.userTrackingMode != .followWithHeading {
            mapView.userTrackingMode = .followWithHeading
        } else {
            mapView.resetNorth()
        }
        
        // We're borrowing this method as a gesture recognizer, so reset selection state.
        mapView.deselectAnnotation(annotation, animated: false)
    }*/
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        // get the custom reuse identifier for this annotation
        let reuseIdentifier = reuseIdentifierForAnnotation(annotation: annotation)
        // try to reuse an existing annotation image, if it exists
        var annotationImage = mapView.dequeueReusableAnnotationImage(withIdentifier: reuseIdentifier)
        
        // if the annotation image hasn‘t been used yet, initialize it here with the reuse identifier
        if annotationImage == nil {
            // lookup the image for this annotation
            let image = imageForAnnotation(annotation: annotation)
            annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: reuseIdentifier)
        }
        
        return annotationImage
    }
    
    // create a reuse identifier string by concatenating the annotation coordinate, title, subtitle
    func reuseIdentifierForAnnotation(annotation: MGLAnnotation) -> String {
        var reuseIdentifier = "\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)"
        if let title = annotation.title, title != nil {
            reuseIdentifier += title!
        }
        if let subtitle = annotation.subtitle, subtitle != nil {
            reuseIdentifier += subtitle!
        }
        return reuseIdentifier
    }
    
    // lookup the image to load by switching on the annotation's title string
    func imageForAnnotation(annotation: MGLAnnotation) -> UIImage {
        var imageName = ""
        if let title = annotation.title, title != nil {
            switch title! {
            case "Cars":
                if let subtitle = annotation.subtitle, subtitle != nil {
                    switch subtitle! {
                    case "offline":
                        imageName = "map_car_icon"
                    case "online":
                        imageName = "map_car_icon"
                    default:
                        imageName = "map_car_icon"
                    }
                }
            case "Trucks":
                if let subtitle = annotation.subtitle, subtitle != nil {
                    switch subtitle! {
                    case "offline":
                        imageName = "map_truck_icon"
                    case "online":
                        imageName = "map_truck_icon"
                    default:
                        imageName = "map_truck_icon"
                    }
                }
            case "":
                if let subtitle = annotation.subtitle, subtitle != nil {
                    switch subtitle! {
                    case "offline":
                        imageName = "map_car_icon"
                    case "online":
                        imageName = "map_car_icon"
                    default:
                        imageName = "map_car_icon"
                    }
                }
            default:
                if let subtitle = annotation.subtitle, subtitle != nil {
                    switch subtitle! {
                    case "offline":
                        imageName = "map_Jeep_icon"
                    case "online":
                        imageName = "map_Jeep_icon"
                    default:
                        imageName = "map_Jeep_icon"
                    }
                }
            }
        }else{
            imageName = "map_location_icon"
        }
        // ... etc.
        return UIImage(named: imageName)!
    }
    
    func mapView(_ mapView: MGLMapView, didSelect annotation: MGLAnnotation) {
        self.viewPopup.isHidden=false
        self.buttonScan.isHidden=false
        let a_coordinate = annotation.coordinate
        let lat = a_coordinate.latitude
        let long = a_coordinate.longitude
        for item in self.assetArray {
            if lat == item.properties?.lat && long == item.properties?.long {
                let pinDetails = item
                print(pinDetails.properties?.licencePlate! as Any)
            }
        }
    }
    
    func mapView(_ mapView: MGLMapView, didDeselect annotation: MGLAnnotation) {
        self.viewPopup.isHidden=true
        self.buttonScan.isHidden=true
    }
}
extension HomeView : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dieselCollectionCell", for: indexPath) as! DieselCollectionCell
        if indexPath.item<3 {
            cell.dieselMeterImage.backgroundColor=UIColor(red: 42.0/255, green: 56.0/255, blue: 98.0/255, alpha: 1.0)
        }else{
            cell.dieselMeterImage.backgroundColor=UIColor.white
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:(collectionView.frame.size.width-20)/5,height:10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.0
    }
}
extension HomeView : UICollectionViewDelegate {
    
}
extension HomeView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
