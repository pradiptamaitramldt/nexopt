//
//  TermsConditionsView.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit
import WebKit

class TermsConditionsView: UIViewController {

    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    var isAccept : Bool = false
    var webView = WKWebView()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let url = URL(string: "https://fleet.nexopt.com/api/user/terms.php")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.navigationDelegate = self
        self.webView.frame = self.viewBG.bounds
        self.viewBG.addSubview(webView)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonMenuAction(_ sender: Any) {
    }
    @IBAction func buttonAcceptTermsAction(_ sender: Any) {
        isAccept = !isAccept
        switch isAccept {
        case true:
            self.checkIcon.image=UIImage(named: "accept_icon")
        case false:
            self.checkIcon.image=UIImage(named: "unselected-icon_")
        }
    }
    @IBAction func buttonContinueAction(_ sender: Any) {
        if isAccept==true {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
            vc.pageFrom = ""
            SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
        }else{
            Utility.showAlert(withMessage: "Please agree to terms & conditions first", onController: self)
        }
        
    }
}
extension TermsConditionsView : WKNavigationDelegate {
    
}
