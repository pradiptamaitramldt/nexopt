//
//  CompletedReportsView.swift
//  Nexopt
//
//  Created by Pradipta on 06/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class CompletedReportsView: UIViewController {

    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var imageDone: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.table_view.delegate=self
        self.table_view.dataSource=self
        self.table_view.estimatedRowHeight = UITableView.automaticDimension
        self.table_view.rowHeight = 44.0
        self.textFieldSearch.delegate=self
        self.buttonDone.isHidden=true
        self.imageDone.isHidden=true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func checkSelectedData() {
        if self.table_view.indexPathsForSelectedRows != nil {
            self.buttonDone.isHidden=false
            self.imageDone.isHidden=false
        }else{
            self.buttonDone.isHidden=true
            self.imageDone.isHidden=true
        }
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonSortAction(_ sender: Any) {
    }
}
extension CompletedReportsView : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "completedReportCell", for: indexPath) as! CompletedReportCell
        cell.selectionStyle=UITableViewCell.SelectionStyle.none
        cell.viewBG.dropViewShadow()
        cell.delegate=self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.checkSelectedData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.checkSelectedData()
    }
    
    /*func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 50.0))
        //let leftLabel = UIButton(frame: CGRect(x: view.bounds.width - 50, y: 0.0, width: 40.0, height: 40.0))
        let leftLabel = UILabel(frame: CGRect(x: 10, y: 0, width: 150, height: 50))
        leftLabel.text="Open Reports"
        leftLabel.textColor=UIColor(red: 65.0/255, green: 65.0/255, blue: 65.0/255, alpha: 1.0)
        leftLabel.font=UIFont(name: "OpenSans-Regular", size: 16)
        leftLabel.textAlignment=NSTextAlignment.left
        
        let rightLabel = UILabel(frame: CGRect(x: view.bounds.width-160, y: 0, width: 150, height: 50))
        rightLabel.text="9th Oct, 2019"
        rightLabel.textColor=UIColor(red: 65.0/255, green: 65.0/255, blue: 65.0/255, alpha: 1.0)
        rightLabel.font=UIFont(name: "OpenSans-Regular", size: 16)
        rightLabel.textAlignment=NSTextAlignment.right
        view.addSubview(rightLabel)
        view.addSubview(leftLabel)
        return view
    }*/
}
extension CompletedReportsView : SendReportDelegate {
    func sendReport(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeView") as! HomeView
        vc.pageFrom = "SendReport"
        SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: true, andCompletion: nil)
    }
}
extension CompletedReportsView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
