//
//  MyMessageView.swift
//  Nexopt
//
//  Created by Pradipta on 05/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class MyMessageView: UIViewController {

    @IBOutlet weak var table_view: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.table_view.delegate=self
        self.table_view.dataSource=self
        self.table_view.estimatedRowHeight = UITableView.automaticDimension
        self.table_view.rowHeight = 90.0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    @IBAction func buttonAddAction(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Messages", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MessageDetailView") as! MessageDetailView
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension MyMessageView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "messageListCell", for: indexPath) as! MessageListCell
        cell.selectionStyle=UITableViewCell.SelectionStyle.none
        cell.viewBG.dropViewShadow()
        return cell
    }
}
extension MyMessageView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Messages", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "MessageDetailView") as! MessageDetailView
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
