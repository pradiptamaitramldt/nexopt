//
//  MessageDetailView.swift
//  Nexopt
//
//  Created by Pradipta on 07/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class MessageDetailView: UIViewController {

    @IBOutlet weak var textFieldMessage: UITextField!
    @IBOutlet weak var viewSendMessage: UIView!
    @IBOutlet weak var viewTextBG: UIView!
    @IBOutlet weak var table_view: UITableView!
    @IBOutlet weak var buttonSendMessage: UIButton!
    @IBOutlet weak var buttonTypeChat: UIButton!
    var lowerViewInitialY : CGFloat = 0
    var tableViewInitialY : CGFloat = 0
    var tableViewInitialHeight : CGFloat = 0
    var textField = UITextField()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.table_view.delegate=self
        self.table_view.dataSource=self
        self.table_view.estimatedRowHeight = UITableView.automaticDimension
        self.table_view.rowHeight = 44.0
        self.viewTextBG.dropViewShadow()
        //self.setKeyboardNotification()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        /*self.lowerViewInitialY = self.viewSendMessage.frame.origin.y
        self.tableViewInitialY = self.table_view.frame.origin.y
        self.tableViewInitialHeight = table_view.frame.height*/
        // This code is for adding a custom comment textfield view above keyboard
        let customView = UIView(frame: self.viewSendMessage.frame)
        let innerCustView = UIView(frame: self.viewTextBG.frame)
        self.textField = UITextField(frame: self.textFieldMessage.frame)
        self.textField.placeholder="Type your reply here"
        self.textField.borderStyle=UITextField.BorderStyle.none
        self.textField.font=UIFont(name: "Gilroy-Regular", size: 14)
        self.textField.delegate=self
        innerCustView.layer.borderWidth=1
        innerCustView.layer.borderColor=UIColor(red: 240.0/255, green: 240.0/255, blue: 240.0/255, alpha: 0.3).cgColor
        innerCustView.layer.cornerRadius=innerCustView.frame.size.height/2
        innerCustView.addSubview(self.textField)
        innerCustView.dropShadow()
        customView.addSubview(innerCustView)
        let sendCommentButton = UIButton(frame: buttonSendMessage.frame)
        sendCommentButton.setImage(UIImage(named: "send_button"), for: UIControl.State.normal)
        sendCommentButton.addTarget(self, action: #selector(self.buttonSendMessage(_:)), for: UIControl.Event.touchUpInside)
        customView.addSubview(sendCommentButton)
        customView.backgroundColor = UIColor.white
        //self.commentView.delegate=self
        self.textFieldMessage.inputAccessoryView=customView
        self.textFieldMessage.delegate=self
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setKeyboardNotification(){
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(MessageDetailView.setLowerViewPositionShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MessageDetailView.setLowerViewPositionHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func setLowerViewPositionShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            print("Keyboard End", keyboardFrame)
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            viewSendMessage.frame.origin.y = lowerViewInitialY - keyboardHeight
            //view_Lower.frame.origin.y = keyboardRectangle.origin.y - view_Lower.frame.height
            table_view.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
        }
    }
    @objc func setLowerViewPositionHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            print("Keyboard End", keyboardFrame)
            //let keyboardRectangle = keyboardFrame.cgRectValue
            //let keyboardHeight = keyboardRectangle.height
            viewSendMessage.frame.origin.y = lowerViewInitialY
            //view_Lower.frame.origin.y = keyboardRectangle.origin.y - view_Lower.frame.height
            table_view.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    @IBAction func buttonMenuAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonSendMessage(_ sender: Any) {
        self.textFieldMessage.resignFirstResponder()
        self.textField.resignFirstResponder()
        self.view.endEditing(true)
    }
    @IBAction func buttonTypeChatAction(_ sender: Any) {
        self.textFieldMessage.becomeFirstResponder()
        self.textField.becomeFirstResponder()
    }
}
extension MessageDetailView : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chatReceiverCell", for: indexPath) as! ChatReceiverCell
            cell.selectionStyle=UITableViewCell.SelectionStyle.none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "chatSenderCell", for: indexPath) as! ChatSenderCell
            cell.selectionStyle=UITableViewCell.SelectionStyle.none
            return cell
        }
    }
}
extension MessageDetailView : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension MessageDetailView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.textField.text != "" {
            self.buttonTypeChat.setTitle(self.textField.text, for: .normal)
            self.buttonTypeChat.setTitleColor(UIColor.black, for: .normal)
        }else{
            self.buttonTypeChat.setTitle("Type your reply here", for: .normal)
            self.buttonTypeChat.setTitleColor(UIColor.lightGray, for: .normal)
        }
    }
}
