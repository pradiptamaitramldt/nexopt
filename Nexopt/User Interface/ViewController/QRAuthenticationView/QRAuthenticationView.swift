//
//  QRAuthenticationView.swift
//  Nexopt
//
//  Created by Pradipta on 07/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class QRAuthenticationView: UIViewController {

    @IBOutlet weak var textFieldDeviceCode: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.textFieldDeviceCode.delegate=self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func buttonMenuAction(_ sender: Any) {
        //SlideNavigationController.sharedInstance().toggleLeftMenu()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonAuthenticationAction(_ sender: Any) {
    }
    @IBAction func buttonBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension QRAuthenticationView : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
