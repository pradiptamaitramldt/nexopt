//
//  QRScannerDriveView.swift
//  Nexopt
//
//  Created by Pradipta on 06/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit
import AVFoundation

class QRScannerDriveView: UIViewController {

    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
    
    @IBOutlet weak var buttonScan: UIButton! {
        didSet {
            buttonScan.setTitle("Scan to Drive", for: .normal)
        }
    }
    @IBOutlet weak var labelManualEntry: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
    
    var isCheck : Bool = false
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                NotificationCenter.default.post(name: Notification.Name(Constants.Strings.kQRScanSuccess), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    var toggleFlash : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.labelManualEntry.sizeToFit()
        self.checkIcon.image=UIImage(named: "unselected-icon_")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    
    @IBAction func buttonScanAction(_ sender: Any) {
        scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
        let buttonTitle = scannerView.isRunning ? "Scan to Drive" : "Scan to Drive"
        (sender as AnyObject).setTitle(buttonTitle, for: .normal)
    }
    @IBAction func buttonMenuAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func buttonTorchAction(_ sender: UIButton) {
        if(toggleFlash){
            //do anything else you want to do.
            self.toggleTorch(on: false)
        }
        else {
            //do anything you want to do.
            self.toggleTorch(on: true)
        }
        toggleFlash = !toggleFlash;
    }
    @IBAction func buttonManualAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "QRCode", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "QRAuthenticationView") as! QRAuthenticationView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func buttonCheckAction(_ sender: Any) {
        if isCheck {
            self.checkIcon.image=UIImage(named: "unselected-icon_")
        }else{
            self.checkIcon.image=UIImage(named: "Selected_Driving_icon")
        }
        isCheck = !isCheck
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func showToast(message : String, seconds: Double = 2.0) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black
        alert.view.alpha = 0.6
        alert.view.layer.cornerRadius = 15
        
        self.present(alert, animated: true)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
            alert.dismiss(animated: true)
        }
    }

    func toggleTorch(on: Bool) {
        guard let device = AVCaptureDevice.default(for: .video) else { return }
        if device.hasTorch {
            do {
                try device.lockForConfiguration()
                if on == true {
                    device.torchMode = .on
                } else {
                    device.torchMode = .off
                }
                device.unlockForConfiguration()
            } catch {
                print("Torch could not be used")
            }
        } else {
            print("Torch is not available")
        }
    }
}
extension QRScannerDriveView : QRScannerViewDelegate {
    func qrScanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "Scan to Drive" : "Scan to Drive"
        buttonScan.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        showToast(message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
    }
}
