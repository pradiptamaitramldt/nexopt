//
//  DieselCollectionCell.swift
//  Nexopt
//
//  Created by Pradipta on 22/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class DieselCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var dieselMeterImage: UIImageView!
}
