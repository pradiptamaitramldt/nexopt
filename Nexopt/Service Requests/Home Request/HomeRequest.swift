//
//  HomeRequest.swift
//  Nexopt
//
//  Created by Pradipta on 26/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit
import Alamofire

class HomeRequest: NSObject {
    let load = Loader()
    var owner_id : String?
    var source_id : String?
    
    init(owner_id: String?) {
        self.owner_id=owner_id
    }
    init(source_id: String?) {
        self.source_id=source_id
    }
    
    private func printJSON(apiName: String, data: Data) {
        print("API name: ", apiName)
        print(String(data: data, encoding: String.Encoding.utf8) ?? "")
    }
    func presentAlertWithTitle(title: String, message : String, vc: UIViewController)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) {
            (action: UIAlertAction) in print("Youve pressed OK Button")
        }
        alertController.addAction(OKAction)
        vc.present(alertController, animated: true, completion: nil)
    }
    
    func fetchAssets<T : APIResponseParentModel>(owner_id: String, vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                load.show(views: vc.view)
            }
            let assetUrl = Constants.APIPath.fetchAssets + Constants.APIPath.tokenValue + "&owner_id=5ced1d24ae03f34c2f67c5c2&offset=0&limit=15"
            
            Alamofire.request(assetUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
                if hud{
                    self.load.hide(delegate: vc)
                }
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: assetUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value!, "", true)
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
    
    func fetchAssetInfo<T : APIResponseParentModel>(source_id: String, vc: UIViewController, hud: Bool, codableType : T.Type,completion: @escaping (_ responce : Any?,_ message : NSString, _ status : Bool) -> ()) {
        
        if Utility.isNetworkReachable() {
            if hud{
                //load.show(views: vc.view)
            }
            let assetUrl = Constants.APIPath.fetchAssetInfo + Constants.APIPath.tokenValue + "&source_id=19105380&offset=0&limit=1"
            
            Alamofire.request(assetUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { response in
                if hud{
                    self.load.hide(delegate: vc)
                }
                switch(response.result){
                case .success(_):
                    self.printJSON(apiName: assetUrl, data: response.result.value!)
                    print(response.result.description)
                    if response.result.value != nil {
                        completion(response.result.value!, "", true)
                    }
                    break
                case .failure(_):
                    print(response.result.error ?? "Fail")
                    self.presentAlertWithTitle(title: "Error!", message: (response.result.error?.localizedDescription)!, vc: vc)
                    break
                }
            }
        }
    }
}
