//  ETUILabelDataBinder.swift
//  Nexopt

import UIKit

class ETUILabelDataBinder: NSObject {

    var model : NSObject
    var uiLabel : UILabel
    var bindProperty : String
    
    init(model : NSObject, uiLabel : UILabel, bindProperty : String) {
        self.model = model
        self.uiLabel = uiLabel
        self.bindProperty = bindProperty
        
        super.init()
        self.model.addObserver(self, forKeyPath: bindProperty, options: .new, context: nil)
        self.uiLabel.addObserver(self, forKeyPath: "text", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "text") {
            
            let currentValue = self.model.value(forKey: self.bindProperty) as? String
            
            if (currentValue != self.uiLabel.text) {
                self.model.setValue(self.uiLabel.text, forKey: self.bindProperty)
            }
        }
        else if (keyPath == bindProperty) {
            
            let currentValue = self.uiLabel.text
            
            if (currentValue != self.model.value(forKey: bindProperty) as? String) {
                self.uiLabel.setValue(self.model.value(forKey: bindProperty), forKey: "text")
            }
        }
    }
}
