//
//  ETTextFieldDataBinder.swift
//  Nexopt
//

import UIKit

class ETTextFieldDataBinder: NSObject {
    
    var model: NSObject
    var textField: UITextField
    var bindProperty: String
    
    init(model: NSObject, textField: UITextField, bindProperty: String)
    {
        self.model = model
        self.textField = textField
        self.bindProperty = bindProperty
        
        super.init()
        self.model.addObserver(self, forKeyPath: bindProperty, options: .new, context: nil)
        self.textField.addObserver(self, forKeyPath: "text", options: .new, context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if(keyPath == "text"){
            
            let currentValue = self.model.value(forKey: self.bindProperty) as? String
            
            if(currentValue != self.textField.text){
                self.model.setValue(self.textField.text, forKey: self.bindProperty)
            }
        }
        else if(keyPath == bindProperty){
            
            let currentValue = self.textField.text
            
            if(currentValue != self.model.value(forKey: bindProperty) as? String){
                self.textField.setValue(self.model.value(forKey: bindProperty), forKey: "text")
            }
        }
    }
}
