//
//  LoginRepository.swift
//  Nexopt
//
//  Created by Pradipta on 25/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LoginRepository: NSObject {
    func loginUser(loginCommand: LoginCommand, vc: UIViewController, completion: @escaping (UserInfo, Bool, NSError?) -> Void) {
        let userId = loginCommand.userID
        let password = loginCommand.password
        let request = LoginRequest(userId: userId!, password: password!)
        request.loginUser(loginCommand: loginCommand, vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(UserInfo.self, from: response! as! Data)
                    let userid = objResponse._id!
                    print("id...\(userid)")
                    completion(objResponse, success, nil)
                    
                } catch let error {
                    print("JSON Parse Error: \(error.localizedDescription)")
                }
            }
        }
    }
}
