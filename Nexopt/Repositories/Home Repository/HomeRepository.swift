//
//  HomeRepository.swift
//  Nexopt
//
//  Created by Pradipta on 26/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class HomeRepository: NSObject {
    func fetchAssets(owner_id: String, vc: UIViewController, completion: @escaping (FetchAssetsResponseModel, Bool, NSError?) -> Void) {
        let request = HomeRequest(owner_id: owner_id)
        request.fetchAssets(owner_id: owner_id, vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(FetchAssetsResponseModel.self, from: response! as! Data)
                    //print("ownerid...\(objResponse._id!)")
                    completion(objResponse, success, nil)
                } catch let DecodingError.dataCorrupted(context){
                    print(context) }
                catch let DecodingError.keyNotFound(key, context){
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath) }
                catch let DecodingError.valueNotFound(value, context){
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath) }
                catch let DecodingError.typeMismatch(type, context){
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath) }
                catch {
                    print("error: ", error)
                }
            }
        }
    }
    
    func fetchAssetInfo(source_id: String, vc: UIViewController, completion: @escaping (FetchAssetInfo, Bool, NSError?) -> Void) {
        let request = HomeRequest(source_id: source_id)
        request.fetchAssetInfo(source_id: source_id, vc: vc, hud: true, codableType: APIResponseParentModel.self) { (response, message, success) in
            if success{
                do {
                    let objResponse = try JSONDecoder().decode(FetchAssetInfo.self, from: response! as! Data)
                    //print("ownerid...\(objResponse._id!)")
                    completion(objResponse, success, nil)
                } catch let DecodingError.dataCorrupted(context){
                    print(context) }
                catch let DecodingError.keyNotFound(key, context){
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath) }
                catch let DecodingError.valueNotFound(value, context){
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath) }
                catch let DecodingError.typeMismatch(type, context){
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath) }
                catch {
                    print("error: ", error)
                }
            }
        }
    }
}
