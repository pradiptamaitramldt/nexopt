//
//  ETWSessionRepository.swift
//  Nexopt
//
//  Created by Abhijit on 10/25/18.
//

import UIKit

class ETWSessionRepository: NSObject {
    
    static let shared : ETWSessionRepository = ETWSessionRepository()
    
    var uid: String?
    var email: String?
    var password: String?
    var token: String?
    
    public func load(){
        //Load from keychain
        
        self.uid = KeychainWrapper.standard.string(forKey: Constants.Keychain.kUid)
        self.password = KeychainWrapper.standard.string(forKey: Constants.Keychain.kPassword)
        self.token = KeychainWrapper.standard.string(forKey: Constants.Keychain.kAuthToken)
    }
    
    public func save(){
        //Save to keychain
        
        if let uid = self.uid{
            
            KeychainWrapper.standard.set(uid, forKey: Constants.Keychain.kUid)
        }
        
        if let password = self.password{
            
            KeychainWrapper.standard.set(password, forKey: Constants.Keychain.kPassword)
        }
        
        if let auth_token = self.token{
            
            KeychainWrapper.standard.set(auth_token, forKey: Constants.Keychain.kAuthToken)
        }
    }
    
    public func clear(){
        //Clear from keychain
        
        KeychainWrapper.standard.removeObject(forKey: Constants.Keychain.kUid)
        KeychainWrapper.standard.removeObject(forKey: Constants.Keychain.kPassword)
        KeychainWrapper.standard.removeObject(forKey: Constants.Keychain.kAuthToken)
    }
}
