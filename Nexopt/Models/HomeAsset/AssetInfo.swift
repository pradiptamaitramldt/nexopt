//
//  AssetInfo.swift
//  Nexopt
//
//  Created by Pradipta on 28/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import Foundation

// MARK: - FetchAssetInfo
class FetchAssetInfo: Codable {
    let id: String?
    let sourceID: Int?
    let vin: String?
    let ownerID: AssetOwnerID?
    let timestamp: Int?
    let timeformat: String?
    let properties: AssetProperties?
    let drive: Int?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case sourceID = "source_id"
        case vin
        case ownerID = "owner_id"
        case timestamp, timeformat, properties, drive
    }
    
    init(id: String?, sourceID: Int?, vin: String?, ownerID: AssetOwnerID?, timestamp: Int?, timeformat: String?, properties: AssetProperties?, drive: Int?) {
        self.id = id
        self.sourceID = sourceID
        self.vin = vin
        self.ownerID = ownerID
        self.timestamp = timestamp
        self.timeformat = timeformat
        self.properties = properties
        self.drive = drive
    }
}

// MARK: - OwnerID
class AssetOwnerID: Codable {
    let oid: String?
    
    enum CodingKeys: String, CodingKey {
        case oid = "$oid"
    }
    
    init(oid: String?) {
        self.oid = oid
    }
}

// MARK: - Properties
class AssetProperties: Codable {
    let sourceID: Int?
    let long, lat: Double?
    let alt, datetimeS, gpsUncertainty: Int?
    let gpsSpeed: Double?
    let gpsNrSat: Int?
    let batteryVoltage: Double?
    let gsmSignalStrength, engSpeedMax, engSpeedAvg: Int?
    let vin: String?
    let speed, engineLoadPcnt, pedalInPcnt: Int?
    let adblueDist: String?
    let odometer, engSpeed: Int?
    
    enum CodingKeys: String, CodingKey {
        case sourceID = "source_id"
        case long, lat, alt
        case datetimeS = "datetime_s"
        case gpsUncertainty = "gps_uncertainty"
        case gpsSpeed = "gps_speed"
        case gpsNrSat = "gps_nr_sat"
        case batteryVoltage = "battery_voltage"
        case gsmSignalStrength = "gsm_signal_strength"
        case engSpeedMax = "eng_speed_max"
        case engSpeedAvg = "eng_speed_avg"
        case vin, speed
        case engineLoadPcnt = "engine_load_pcnt"
        case pedalInPcnt = "pedal_in_pcnt"
        case adblueDist = "adblue_dist"
        case odometer
        case engSpeed = "eng_speed"
    }
    
    init(sourceID: Int?, long: Double?, lat: Double?, alt: Int?, datetimeS: Int?, gpsUncertainty: Int?, gpsSpeed: Double?, gpsNrSat: Int?, batteryVoltage: Double?, gsmSignalStrength: Int?, engSpeedMax: Int?, engSpeedAvg: Int?, vin: String?, speed: Int?, engineLoadPcnt: Int?, pedalInPcnt: Int?, adblueDist: String?, odometer: Int?, engSpeed: Int?) {
        self.sourceID = sourceID
        self.long = long
        self.lat = lat
        self.alt = alt
        self.datetimeS = datetimeS
        self.gpsUncertainty = gpsUncertainty
        self.gpsSpeed = gpsSpeed
        self.gpsNrSat = gpsNrSat
        self.batteryVoltage = batteryVoltage
        self.gsmSignalStrength = gsmSignalStrength
        self.engSpeedMax = engSpeedMax
        self.engSpeedAvg = engSpeedAvg
        self.vin = vin
        self.speed = speed
        self.engineLoadPcnt = engineLoadPcnt
        self.pedalInPcnt = pedalInPcnt
        self.adblueDist = adblueDist
        self.odometer = odometer
        self.engSpeed = engSpeed
    }
}
