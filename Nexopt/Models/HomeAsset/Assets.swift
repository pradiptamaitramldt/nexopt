// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let fetchAssetsResponseModel = try? newJSONDecoder().decode(FetchAssetsResponseModel.self, from: jsonData)

import Foundation

// MARK: - FetchAssetsResponseModelElement
class FetchAssetsResponseModelElement: Codable {
    let id: String?
    let name: PurpleName?
    let sourceID: Int?
    let ownerID: OwnerID?
    let timestamp: Int?
    let properties: Properties?
    let search: [Int]?
    let formValues: FormValues?
    let guid: String?
    let vin: String?
    let status: Status?
    let info: Info?
    let propertiesID: PropertiesID?
    let connection, service: Int?
    let widgetField, widgetFieldGUID, locationID, locationName: String?
    let assetID: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case sourceID = "source_id"
        case ownerID = "owner_id"
        case timestamp, properties, search
        case formValues = "form_values"
        case guid, vin, status, info
        case propertiesID = "properties_id"
        case connection, service
        case widgetField = "widget_field__"
        case widgetFieldGUID = "widget_field__guid_"
        case locationID = "location_id"
        case locationName = "location_name"
        case assetID = "asset_id"
    }
    
    init(id: String?, name: PurpleName?, sourceID: Int?, ownerID: OwnerID?, timestamp: Int?, properties: Properties?, search: [Int]?, formValues: FormValues?, guid: String?, vin: String?, status: Status?, info: Info?, propertiesID: PropertiesID?, connection: Int?, service: Int?, widgetField: String?, widgetFieldGUID: String?, locationID: String?, locationName: String?, assetID: String?) {
        self.id = id
        self.name = name
        self.sourceID = sourceID
        self.ownerID = ownerID
        self.timestamp = timestamp
        self.properties = properties
        self.search = search
        self.formValues = formValues
        self.guid = guid
        self.vin = vin
        self.status = status
        self.info = info
        self.propertiesID = propertiesID
        self.connection = connection
        self.service = service
        self.widgetField = widgetField
        self.widgetFieldGUID = widgetFieldGUID
        self.locationID = locationID
        self.locationName = locationName
        self.assetID = assetID
    }
}

// MARK: - FormValues
class FormValues: Codable {
    let name: NameEnum?
    let sourceID, guid: Int?
    let dateStart, dateEnd: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case sourceID = "source_id"
        case guid
        case dateStart = "date_start"
        case dateEnd = "date_end"
    }
    
    init(name: NameEnum?, sourceID: Int?, guid: Int?, dateStart: String?, dateEnd: String?) {
        self.name = name
        self.sourceID = sourceID
        self.guid = guid
        self.dateStart = dateStart
        self.dateEnd = dateEnd
    }
}

enum NameEnum: String, Codable {
    case fahrtenbuch = "Fahrtenbuch"
}

// MARK: - Info
class Info: Codable {
    let category, country: String?
    
    init(category: String?, country: String?) {
        self.category = category
        self.country = country
    }
}

enum PurpleName: Codable {
    case integer(Int)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(PurpleName.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for PurpleName"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}

// MARK: - OwnerID
class OwnerID: Codable {
    let oid: OID?
    
    enum CodingKeys: String, CodingKey {
        case oid = "$oid"
    }
    
    init(oid: OID?) {
        self.oid = oid
    }
}

enum OID: String, Codable {
    case the5Ced1D24Ae03F34C2F67C5C2 = "5ced1d24ae03f34c2f67c5c2"
}

// MARK: - Properties
class Properties: Codable {
    let lat, long: Double?
    let imei: Int?
    let firmwareVersion: String?
    let batteryVoltage: Double?
    let vin: String?
    let speed: Double?
    let licencePlate, assignedDrivers, cubicCapacity, engine: String?
    let engineName, fuelType, manufacturer, model: String?
    let powerKw, registeredDateInput, tags, taxDeductibleCheckbox: String?
    let threadMeasure, tireRim, tireSize, transmission: String?
    let trim: String?
    let userID: OID?
    let winterTireChange: String?
    let year: PurpleName?
    let odometer: Double?
    let fuel, gpsUncertainty, gsmSignalStrength, gpsNrSat: Int?
    let gpsSpeed: Double?
    let imagePath: String?
    let gpsMode, intakeAirTempDeg: Int?
    
    enum CodingKeys: String, CodingKey {
        case lat, long, imei
        case firmwareVersion = "firmware_version"
        case batteryVoltage = "battery_voltage"
        case vin, speed
        case licencePlate = "licence_plate"
        case assignedDrivers = "assigned_drivers"
        case cubicCapacity = "cubic_capacity"
        case engine
        case engineName = "engine_name"
        case fuelType = "fuel_type"
        case manufacturer, model
        case powerKw = "power_kw"
        case registeredDateInput = "registered_date_input"
        case tags
        case taxDeductibleCheckbox = "tax_deductible_checkbox"
        case threadMeasure = "thread_measure"
        case tireRim = "tire_rim"
        case tireSize = "tire_size"
        case transmission, trim
        case userID = "user_id"
        case winterTireChange = "winter_tire_change"
        case year, odometer, fuel
        case gpsUncertainty = "gps_uncertainty"
        case gsmSignalStrength = "gsm_signal_strength"
        case gpsNrSat = "gps_nr_sat"
        case gpsSpeed = "gps_speed"
        case imagePath = "image_path"
        case gpsMode = "gps_mode"
        case intakeAirTempDeg = "intake_air_temp_deg"
    }
    
    init(lat: Double?, long: Double?, imei: Int?, firmwareVersion: String?, batteryVoltage: Double?, vin: String?, speed: Double?, licencePlate: String?, assignedDrivers: String?, cubicCapacity: String?, engine: String?, engineName: String?, fuelType: String?, manufacturer: String?, model: String?, powerKw: String?, registeredDateInput: String?, tags: String?, taxDeductibleCheckbox: String?, threadMeasure: String?, tireRim: String?, tireSize: String?, transmission: String?, trim: String?, userID: OID?, winterTireChange: String?, year: PurpleName?, odometer: Double?, fuel: Int?, gpsUncertainty: Int?, gsmSignalStrength: Int?, gpsNrSat: Int?, gpsSpeed: Double?, imagePath: String?, gpsMode: Int?, intakeAirTempDeg: Int?) {
        self.lat = lat
        self.long = long
        self.imei = imei
        self.firmwareVersion = firmwareVersion
        self.batteryVoltage = batteryVoltage
        self.vin = vin
        self.speed = speed
        self.licencePlate = licencePlate
        self.assignedDrivers = assignedDrivers
        self.cubicCapacity = cubicCapacity
        self.engine = engine
        self.engineName = engineName
        self.fuelType = fuelType
        self.manufacturer = manufacturer
        self.model = model
        self.powerKw = powerKw
        self.registeredDateInput = registeredDateInput
        self.tags = tags
        self.taxDeductibleCheckbox = taxDeductibleCheckbox
        self.threadMeasure = threadMeasure
        self.tireRim = tireRim
        self.tireSize = tireSize
        self.transmission = transmission
        self.trim = trim
        self.userID = userID
        self.winterTireChange = winterTireChange
        self.year = year
        self.odometer = odometer
        self.fuel = fuel
        self.gpsUncertainty = gpsUncertainty
        self.gsmSignalStrength = gsmSignalStrength
        self.gpsNrSat = gpsNrSat
        self.gpsSpeed = gpsSpeed
        self.imagePath = imagePath
        self.gpsMode = gpsMode
        self.intakeAirTempDeg = intakeAirTempDeg
    }
}

// MARK: - PropertiesID
class PropertiesID: Codable {
    let manufacturer: PurpleName?
    let model, fuelType, transmission: Int?
    
    enum CodingKeys: String, CodingKey {
        case manufacturer, model
        case fuelType = "fuel_type"
        case transmission
    }
    
    init(manufacturer: PurpleName?, model: Int?, fuelType: Int?, transmission: Int?) {
        self.manufacturer = manufacturer
        self.model = model
        self.fuelType = fuelType
        self.transmission = transmission
    }
}

enum Status: String, Codable {
    case assigned = "Assigned"
    case unassigned = "Unassigned"
}

typealias FetchAssetsResponseModel = [FetchAssetsResponseModelElement]


