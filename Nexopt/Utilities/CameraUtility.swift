import UIKit
import AVFoundation

class CameraUtility: NSObject {

    typealias AuthorizationBlock = (NSError?, Bool) -> Void
    
    // Completion block for camera access authorization
    private var cameraAuthBlock  : AuthorizationBlock?
    
    // MARK: - Public
    /// Camera permissions must be authroized by user to begin the scan
    /// - Parameter completion: completion with status or error if any
    
    func requestCameraPermissions(_ completion:@escaping AuthorizationBlock) {
        self.cameraAuthBlock = completion
        
        if isCameraAvailable() {
            let error = NSError(domain: "Camera Exception", code: 100, userInfo: [NSLocalizedDescriptionKey: "Camera not available"])
            self.notifyCameraAccessCompletion(error: error, success: false)
        }
        self.cameraAuthorization(completion)
    }
    
    func hasCameraPermissions() -> Bool {
        return (AVCaptureDevice.authorizationStatus(for: .video) == .authorized)
    }
    
    func isCameraAvailable() -> Bool {
        return (AVCaptureDevice.default(for: .video) != nil)
    }
    
    // MARK: - Private
    /// Capture device is nil if status is AVAuthorizationStatusRestricted
    /// - Returns: Bool
    
    private func cameraAuthorization(_ completion:@escaping AuthorizationBlock) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.notifyCameraAccessCompletion(error: nil, success: true)
            break
        case .denied:
            let error = NSError(domain: "Camera Exception", code: 100, userInfo: [NSLocalizedDescriptionKey: "Camera permissions denied. You can authorize camera permissions from Settings"])
            self.notifyCameraAccessCompletion(error: error, success: false)
            break
        case .notDetermined:
            self.requestAccess(_ :completion)
            break
        case .restricted:
            let error = NSError(domain: "Camera Exception", code: 100, userInfo: [NSLocalizedDescriptionKey: "Camera permissions restricted"])
            self.notifyCameraAccessCompletion(error: error, success: false)
            break
        }
    }
    
    private func notifyCameraAccessCompletion(error: NSError?, success: Bool) {
        if let block = self.cameraAuthBlock {
            DispatchQueue.main.async {
                block(error,success)
            }
        }
    }
    
    /// Request for camera permissions for video
    private func requestAccess(_ completion:@escaping AuthorizationBlock) {
        AVCaptureDevice.requestAccess(for: .video) { (success) in
            if success {
                self.notifyCameraAccessCompletion(error: nil, success: true)
            }
            else {
                let error = NSError(domain: "Camera Exception", code: 100, userInfo: [NSLocalizedDescriptionKey: "Camera permissions denied. You can authorize camera permissions from Settings"])
                self.notifyCameraAccessCompletion(error: error, success: false)
            }
        }
    }
}
