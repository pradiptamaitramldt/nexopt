import UIKit
class ValueChangeNotificationHub {
    
    typealias ValueChangeBlock = (_ value: Any) -> Void
    
    // Identifier : Block
    private typealias TokenBlock = Dictionary<Int,ValueChangeBlock>
    // {Key : TokenBlock}
    private var observers : Dictionary<String,TokenBlock> = [:]

    // {Key : Value(any)}
    private var values    : Dictionary<String, Any> = [:]

    // MARK: Public Interface
    func addObserver(key: String, block: @escaping ValueChangeBlock) throws -> Int {
        // validate empty key
        if key.count == 0 {
            throw NSError(domain: "Observer", code: 100, userInfo: [NSLocalizedDescriptionKey:"Failed to add observer"])
        }
        return self.addBlock(key: key, block: block)
    }

    func getValue(_ key: String)-> Any {
        // Get value for the supplied key
        return self.values[key] as Any
    }
    
    func removeObserver(_ identifier: Int, key: String) {
        // Find observer for the identifier and remove it from the list
        if var tokeObservers = self.observers[key], tokeObservers.keys.contains(identifier) {
            tokeObservers.removeValue(forKey: identifier)
            self.observers[key] = tokeObservers
        }
    }

    //Removes all the observers for the key
    func removeObservers(_ key: String) {
        // Validate if key exist in the observers list and Remove key. Removing key also removes the observers for the supplied key
        if let _ = self.observers[key] {
            self.observers.removeValue(forKey: key)
        }
    }
    
    func updateValue(key: String, value: Any) {
        // Update value for the supplied key and notifies all the key observers that value is updated
        self.values[key] = value
        self.notifyKey(key: key, newValue: value)
    }
    
    func removeValue(key: String) {
        // Remove value for the supplied key from the key values list
        self.values.removeValue(forKey: key)
    }
    
    // MARK: Private Interface
    // Add Block with the new identifier to the observers list for the supplied key.
    private func addBlock(key: String, block: @escaping ValueChangeBlock) -> Int {
        var count = 0
        if let tokenObserversCount = self.observers[key]?.count {
            count = tokenObserversCount
        }
        // Generate unique identifier
        let identifier = Int(Date().timeIntervalSince1970) + count
        // Create observer for the supplied parameters
        // block against identifier.
        let tokenBlock : TokenBlock = [identifier : block]
        // Update new observer current token blocks.
        if var tokens = self.observers[key] {
            tokens[identifier] = block
            self.observers[key] = tokens
        }
        else {
            // Add new observer for key
            self.observers[key] = tokenBlock
        }
        return identifier
    }
    
    // Identify the observers for the key and update values for all the observer's observing the key.
    private func notifyKey(key: String, newValue: Any) {
        if let tokenBlock = self.observers[key] {
            // Notify all observers for new value
            for key in tokenBlock.keys {
                if let block = tokenBlock[key] {
                    DispatchQueue.main.async {
                        block(newValue)
                    }
                }
            }
        }
    }
}
