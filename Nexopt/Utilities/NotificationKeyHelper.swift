struct NotificationKeyHelper {
    
    static func downloadStatusKeyForFilename(fileName: String?)-> String? {
        guard let fileName = fileName else {
            return nil
        }
        return String(format: Constants.NotificationKeys.fileDownloadStatusKey, fileName)
    }
    
    static func uploadStatusKeyForFilename(fileName: String?)-> String? {
        guard let fileName = fileName else {
            return nil
        }
        return String(format: Constants.NotificationKeys.fileUploadStatusKey, fileName)
    }
}

