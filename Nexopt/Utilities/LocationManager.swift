//
//  LocationManager.swift
//  Nexopt

import UIKit
import CoreLocation

class LocationManager: NSObject {
    
    typealias LocationRefreshBlock = (_ result : CLLocation?, _ error : NSError?) -> Void
    typealias AddressBlock = (_ location : CLLocation?, _ placeMark: CLPlacemark?, _ error: NSError?) -> Void
    
    var isAuthorized : Bool {
        get{
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .authorizedWhenInUse, .authorizedAlways, .notDetermined :
                    return true
                case .restricted, .denied :
                    return false
                }
            }
            return false
        }
    }
    
    private var locationManager : CLLocationManager? {
        didSet {
            locationManager?.desiredAccuracy  = kCLLocationAccuracyBest
            locationManager?.delegate         = self
        }
    }
    private var location : CLLocation?
    private var placeMark : CLPlacemark?

    private var addressBlock : AddressBlock?
    
    func getCurrentAddress(_ addressBlock: @escaping AddressBlock)  {
        
        self.addressBlock = addressBlock
        
        if locationManager == nil {
            locationManager = CLLocationManager()
        }
        
        if isAuthorized == true {
            self.locationManager?.requestLocation()
        }
        else {
            self.locationManager?.requestWhenInUseAuthorization()
        }
    }
    
    private func notifyAddressBlockCompletion(_ error: NSError?) {
        if let block = self.addressBlock {
            block(self.location, self.placeMark,error)
        }
    }
}

extension LocationManager  : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        self.location = locations.count > 0 ? locations.first : nil

        if let location = self.location {

            let geoCoder = CLGeocoder()

            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                
                if let placeMarks = placemarks, placeMarks.count > 0 {
                    self.placeMark = placeMarks.first
                    self.notifyAddressBlockCompletion(nil)
                }
                else {
                    let error : NSError  = (error != nil) ? error! as NSError :  NSError(domain: "Nexopt", code: 1, userInfo: [NSLocalizedDescriptionKey: "Failed to fetch location address"])
                    // TODO: log error
                    self.notifyAddressBlockCompletion(error)
                }
                
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        let error = error as NSError
        // TODO: Log error
        self.notifyAddressBlockCompletion(error)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            locationManager?.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            locationManager?.requestLocation()
            break
        case .authorizedAlways:
            // If always authorized
            locationManager?.requestLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            let error = NSError(domain: "Nexopt", code: 1, userInfo: [NSLocalizedDescriptionKey: "Permission Denied"])
            self.notifyAddressBlockCompletion(error)
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            let error = NSError(domain: "Nexopt", code: 1, userInfo: [NSLocalizedDescriptionKey: "Permission Denied"])
            self.notifyAddressBlockCompletion(error)
            break
        }
    }
}
