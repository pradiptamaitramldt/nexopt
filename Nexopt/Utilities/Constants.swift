//
//  Constants.swift
//  Nexopt
//

import Foundation
import UIKit
/* Nexopt Constants */
struct Constants {
    
    struct Strings {
        // Alert Title
        static let AppName = "Nexopt"
        static let kQRScanSuccess = "QRScanSuccess"
        static let OwnerID = "owner_id"
        
        // Alert Action Title
        static let yesAction        = "Yes"
        static let noAction         = "No"
        static let okAction         = "Ok"
        static let cancelAction     = "Cancel"
        
        // Alert Messages
        static let logoutMessage = "Are you sure you want to logout?"
        
        static let cameraPermissionRequired        = "It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. Please go to settings and enable camera in \(AppName) app"
        static let cameraNotAvailable              = "Camera is not available on this Device"
        
        static let noConnectionMessage          = "Could not connect to the server. Please check your internet connection"
        static let enableLocationMessage        = "Allow Location Access from Location Services in Settings."
    }
    
    struct AppLaunched {
        static let AppLaunchedKey = "AppLaunched"
    }
    
    struct StoryboardIdentifiers {
        static let main             = "Main"
        static let myProfile        = "MyProfile"
        static let home             = "Home"
        static let messages         = "Messages"
        static let qrCode           = "QRCode"
        static let reports          = "Reports"
    }
    
    struct ViewControllerIdentifiers {
        static let nexoptLoginView            = "LoginView"
        // Master Data Setup
        static let nexoptTermsConditionView   = "TermsConditionsView"
        // Cases
        static let nexoptLeftMenu             = "LeftMenu"
        static let nexoptSideMenuRootView     = "SideMenuRootView"
        static let filterViewController       = "FilterViewController"
        static let nexoptMyProfileView        = "MyProfileView"
        static let nexoptHomeView             = "HomeView"
        
        // Master
        static let nexoptMyMessageView        = "MyMessageView"
        static let nexoptMessageDetailView    = "MessageDetailView"
        static let nexoptQRScannerDriveView   = "QRScannerDriveView"
        
        // Case Info
        static let nexoptQRAuthenticationView = "QRAuthenticationView"
        static let nexoptCompletedReportsView = "CompletedReportsView"
        static let nexoptReportsView          = "ReportsView"
    }
    
    struct Color {
        //static let themeRedColor               =  UIColor(red: 194.0/255.0, green: 49.0/255.0, blue: 52.0/255.0, alpha: 1.0)
        
        // TODO: Read Below <<< DISCUSS >>>
        /* FIRST VERIFY COLOR ON DEVICE, then -
         1. Constants.Color.themeBlueColor is rgb(30,128,239)
         2. navigationBar.barTintColor appears darker on simulator while setting color programatically, which is rgb(27,104,235)
         3. As of now navigationBar.barTintColor is rgb(33,150,243) which matches the requirement.
         4. This is happening bacause of 'sRGB' color profile and 'Generic RGB' colors, both has different rgb values for colors.
         5. DISCUSS and VERIFY this usage.
         */
        
        static let themeBlueColor               = UIColor(red: 30.0/255.0, green: 128.0/255.0, blue: 239.0/255.0, alpha: 1.0)
        static let sRGBThemeBlueColor           = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        static let sRGBisTranslucentThemeBlueColor = UIColor(red: 0.0/255.0, green: 133.0/255.0, blue: 243.0/255.0, alpha: 1.0)
        
        static let swipeOptionRed   = UIColor(red: 239.0/255.0, green: 83.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        static let swipeOptionGreen = UIColor(red: 32.0/255.0, green: 121.0/255.0, blue: 40.0/255.0, alpha: 1.0)
        static let swipeOptionGray  = UIColor(red: 173.0/255.0, green: 173.0/255.0, blue: 173.0/255.0, alpha: 1.0)
        static let swipeOptionBlue  = UIColor(red: 56.0/255.0, green: 91.0/255.0, blue: 191.0/255.0, alpha: 1.0)
    }
    
    struct APIPath {
        static let webServer      = "https://fleet.nexopt.com/api";
        
        static let baseURL        = webServer
        
        static let userLogin      = baseURL + "/user/login.php?"
        static let userInfo       = baseURL + "/user/info.php?"
        static let fetchAssets    = baseURL + "/asset_user.php?"
        static let fetchAssetInfo = baseURL + "/asset_items.php?"
        
        // Add User
        static let createUser = baseURL + "/user?part=UserProfileAdd"
        static let deleteUser = baseURL + "/user?part=UserProfileDelete"
        
        // FCM Token
        static let addFCMRegistrationToken      = baseURL + "/user?part=FcmToken&register=true"
        //static let addFCMRegistrationToken      = baseURL + "/user?part=FcmToken"
        static let deleteFCMRegistrationToken   = baseURL + "/user?part=FcmToken&register=false"
        static let tokenValue                   = "token=5c7e877cae03f34e6e436f32"
        
        // Master Data
        static private let masterFetch = "/masterValues.fetch?part="
        static let getStates            = baseURL + masterFetch + "states"
        static let getEyeColors         = baseURL + masterFetch + "eyeColors"
        static let getHairColors        = baseURL + masterFetch + "hairColors"
        static let getRaces             = baseURL + masterFetch + "Race"
        static let getUCR               = baseURL + masterFetch + "UCR"
        static let getIncidentType      = baseURL + masterFetch + "IncidentType"
        static let getIncidentSubType   = baseURL + masterFetch + "IncidentSubType"
        static let getEvidenceType      = baseURL + masterFetch + "EvidenceType"
        static let getGenders           = baseURL + masterFetch + "Gender"
        static let getDisposition       = baseURL + masterFetch + "Disposition"
        static let getPartyCodes        = baseURL + masterFetch + "PartyCode"
        static let getAlertTypes        = baseURL + masterFetch + "AlertType"
        static let getPartyTypes        = baseURL + masterFetch + "PartyType"
        static let getLocations         = baseURL + masterFetch + "Locations"
        static let getEvidenceTransferTypes = baseURL + masterFetch + "FetchTransferType"
        static let getEvidenceUnitTypes     = baseURL + "/evidence.fetch?part=EvidenceUnitType"
        static let getEvidenceUnitSubTypes  = baseURL + "/evidence.fetch?part=EvidenceUnitSubType"
        static let getUsers                 = baseURL + "/user?part=" + "Users"
        
        static let createIncidentType       = baseURL + "/masterValues.update?part=" + "InsertIncidentTypes"
        static let updateIncidentType       = baseURL + "/masterValues.update?part=" + "UpdateIncidentType"
        static let deleteIncidentType       = baseURL + "/masterValues.update?part=" + "DeleteIncidentType"
        
        static let createIncidentSubType   = baseURL + "/masterValues.update?part=" + "InsertIncidentSubTypes"
        static let updateIncidentSubType   = baseURL + "/masterValues.update?part=" + "UpdateIncidentSubType"
        static let deleteIncidentSubType   = baseURL + "/masterValues.update?part=" + "deleteIncidentSubType"
        
        static let createState       = baseURL + "/masterValues.update?part=" + "insertStates"
        static let updateState       = baseURL + "/masterValues.update?part=" + "updateStates"
        static let deleteState       = baseURL + "/masterValues.update?part=" + "DeleteStates"
        
        static let createPartyType  = baseURL + "/masterValues.update?part=" + "InsertPartyType"
        static let updatePartyType  = baseURL + "/masterValues.update?part=" + "UpdatePartyType"
        static let deletePartyType  = baseURL + "/masterValues.update?part=" + "DeletePartyType"
        
        static let createHairColor   = baseURL + "/masterValues.update?part=" + "insertHairColors"
        static let updateHairColor   = baseURL + "/masterValues.update?part=" + "updateHairColor"
        static let deleteHairColor   = baseURL + "/masterValues.update?part=" + "deleteHairColor"
        
        static let createEyeColor   = baseURL + "/masterValues.update?part=" + "insertEyeColors"
        static let updateEyeColor   = baseURL + "/masterValues.update?part=" + "UpdateEyeColor"
        static let deleteEyeColor   = baseURL + "/masterValues.update?part=" + "deleteEyeColor"
        
        static let createRace   = baseURL + "/masterValues.update?part=" + "InsertRace"
        static let updateRace   = baseURL + "/masterValues.update?part=" + "UpdateRace"
        static let deleteRace   = baseURL + "/masterValues.update?part=" + "DeleteRace"
        
        static let createPartyCode   = baseURL + "/masterValues.update?part=" + "InsertPartyCode"
        static let updatePartyCode   = baseURL + "/masterValues.update?part=" + "UpdatePartyCode"
        static let deletePartyCode   = baseURL + "/masterValues.update?part=" + "DeletePartyCode"
        
        static let createEvidenceType       = baseURL + "/masterValues.update?part=" + "InsertEvidenceType"
        static let updateEvidenceType       = baseURL + "/masterValues.update?part=" + "UpdateEvidenceType"
        static let deleteEvidenceType       = baseURL + "/masterValues.update?part=" + "DeleteEvidenceType"
        
        static let createUCR       = baseURL + "/masterValues.update?part=" + "InsertUCR"
        static let updateUCR       = baseURL + "/masterValues.update?part=" + "UpdateUCR"
        static let deleteUCR       = baseURL + "/masterValues.update?part=" + "DeleteUCR"
        
        static let createDisposition       = baseURL + "/masterValues.update?part=" + "InsertDisposition"
        static let updateDisposition       = baseURL + "/masterValues.update?part=" + "UpdateDisposition"
        static let deleteDisposition       = baseURL + "/masterValues.update?part=" + "DeleteDisposition"
        
        static let createEvidenceUnitType  = baseURL + "/evidence.update?part=" + "InsertEvidenceUnitType"
        static let updateEvidenceUnitType  = baseURL + "/evidence.update?part=" + "UpdateEvidenceUnitType"
        static let deleteEvidenceUnitType  = baseURL + "/evidence.update?part=" + "DeleteEvidenceUnitType"
        
        static let createEvidenceUnitSubType  = baseURL + "/evidence.update?part=" + "InsertEvidenceUnitSubType"
        static let updateEvidenceUnitSubType  = baseURL + "/evidence.update?part=" + "UpdateEvidenceUnitSubType"
        static let deleteEvidenceUnitSubType  = baseURL + "/evidence.update?part=" + "DeleteEvidenceUnitSubType"
        
        static let createEvidenceLocation = baseURL + "/masterValues.update?part=" + "InsertsLocations"
        static let updateEvidenceLocation = baseURL + "/masterValues.update?part=" + "LocationUpdate"
        static let deleteEvidenceLocation = baseURL + "/masterValues.update?part=" + "DeleteLocations"
        
        // Case
        static let getCases                 = baseURL + "/case.fetch?part=" + "Case"
        static let getCaseDetails           = baseURL + "/case.fetch?part=" + "CaseDetails"
        
        static let createCase               = baseURL + "/case.update?part=" + "InsertCase"
        static let updateCase               = baseURL + "/case.update?part=" + "UpdateCase"
        static let deleteCase               = baseURL + "/criminalCase.update?part=" + "DeleteCase"
        
        // Party
        static let createParty              = baseURL + "/party.update?part=" + "InsertParty"
        static let updateParty              = baseURL + "/party.update?part=" + "PartyUpdate"
        static let deleteParty              = baseURL + "/party.update?part=" + "DeleteParty"
        
        // Evidence
        static let createEvidence           = baseURL + "/evidence.update?part=" + "InsertEvidence"
        static let updateEvidence           = baseURL + "/evidence.update?part=" + "EvidenceUpdate"
        static let deleteEvidence           = baseURL + "/evidence.update?part=" + "DeleteEvidence"
        
        // Evidence Transfer
        static let getEvidenceTransferHistory = baseURL + masterFetch + "FetchTransferEvidence"
        static let createEvidenceTransfer = baseURL + "/masterValues.update?part=" + "InsertTransferEvidence"
        static let updateEvidenceTransfer = baseURL + "/masterValues.update?part=" + "UpdateTransferEvidence"
        //static let updateEvidenceTransferStatus = baseURL + "/masterValues.update?part=" + "EvidenceTransferRequest"
        static let updateEvidenceTransferStatus = baseURL + "/masterValues.update?part=" + "EvidenceTransferBulkRequest"
        static let createBulkEvidenceTransfer = baseURL + "/masterValues.update?part=" + "BulkTransferEvidence"
        
        // Remark
        static let createRemark             = baseURL + "/masterValues.update?part=" + "InsertRemarks"
        static let updateRemark             = baseURL + "/masterValues.update?part=" + "UpdateRemarks"
        
        // Evidence Unit
        static let createEvidenceUnit        = baseURL + "/evidence.update?part=" + "InsertEvidenceUnits"
        static let updateEvidenceUnit        = baseURL + "/evidence.update?part=" + "UpdateEvidencesUnits"
        static let deleteEvidenceUnit        = baseURL + "/evidence.update?part=" + "DeleteEvidenceUnit"
        
        // Download Image
        static let downloadImage             = baseURL + "/getImage?part=GetImage&Type="
        static let downloadPartyTypeImage    = downloadImage + "Party&ID="
        static let downloadEvidenceTypeImage = downloadImage + "Evidence&ID="
        static let downloadEvidenceTransferSignature = downloadImage + "Signature&ID="
        // Update Default Picture
        static let setDefaultPartyPicture    = baseURL + "/imageIsDefault?part=IsDefaultPartyImage"
        static let setDefaultEvidencePicture = baseURL + "/imageIsDefault?part=IsDefaultEvidenceImage"
        
        // Upload Image
        static let uploadPartyImage          = baseURL + "/uploadImage?part=UploadImage&Type=Party"
        static let uploadEvidenceImage       = baseURL + "/uploadImage?part=UploadImage&Type=Evidence"
        static let uploadEvidenceTransferSignature = baseURL + "/uploadImage?part=UploadImage&Type=Signature"
        
        // Alerts
        //static let getAlerts                 = baseURL + masterFetch + "FetchAlerts&ReadStatus=0&Snooze=0"
        static let getAlerts                 = baseURL + masterFetch + "FetchAlerts"
        static let getAlertTransfer                 = baseURL + masterFetch + "FetchAlerts&onlyTransfer:True"
        static let readOrSnoozAlert = baseURL + "/masterValues.update?part=" + "ReadSnoozeAlerts"
        static let createAlert = baseURL + "/masterValues.update?part=" + "CreateFutureAlertMobile"
        static let deleteAlert = baseURL + "/masterValues.update?part=" + "DeleteAlerts"
    }
    
    struct Keychain {
        static let kUid = "uid"
        static let kDsn = "email"
        static let kPassword = "password"
        static let kAuthToken = "token"
    }
    
    struct AlertTypeID {
        static let futureAlertTypeID = "FutureAlertTypeID"
        static let transferInitiatedAlertTypeID = "TransferInitiatedAlertTypeID"
    }
    
    struct UserType {
        static let GUEST = 0
        static let REGULAR = 1
        static let ADMIN = 2
    }
    
    struct DateFormat {
        public static let FILE_NAME_FORMAT = "yyyy_MM_dd_HH_mm_ss"
        public static let FILE_NAME_FORMAT_ONLY_DIGITS = "yyyyMMddHHmmss";
        public static let FILE_NAME_DATE = "yyyy_MM_dd";
        public static let FILE_NAME_TIME = "HH_mm_ss";
        
        public static let DD_MM_YYYY = "dd:MM:yyyy";
        public static let DD_MM_YYYY_SLASH = "dd/MM/yyyy";
        public static let DD_MM_YYYY_HH_MM_SS = "dd:MM:yyyy HH:mm:ss";
        public static let DD_MM_YYYY_HH_MM_SS_A = "dd:MM:yyyy hh:mm:ss a";
        public static let DD_MMM_YYYY = "dd MMM yyyy";
        public static let DD_MMM_YYYY_WITH_COMMA = "dd MMM, yyyy";
        public static let DD_MMMM = "dd MMMM";
        public static let DD_MMMM_YYYY = "dd MMMM yyyy";
        public static let EEEE_D_MMM_YYYY = "EEEE, d MMM yyyy";
        public static let EEE = "EEE";
        public static let EEEE = "EEEE";
        public static let EEE_MMM_DD = "EEE, MMM dd";
        public static let EEE_MMM_DD_YYYY = "EEE, MMM dd, yyyy";
        public static let EEE_MMM_DD_YYYY_HH_MM_SS_A = "EEE, MMM dd, yyyy hh:mm:ss a";
        
        public static let H_MM_A = "h:mm a";
        public static let HH_MM_A = "hh:mm a";
        public static let HH_MM_A_Z = "hh:mm a z";
        public static let HH_MM_SS = "HH:mm:ss";
        public static let HH_MM_SS_A = "hh:mm:ss a";
        
        public static let MM_DD_YYYY = "MM-dd-yyyy";
        public static let MM_DD_YYYY_HH_MM_A = "MM-dd-yyyy hh:mm a";
        public static let MM_DD_YYYY_HH_MM = "MM-dd-yyyy HH:mm";
        public static let MM_DD_YYYY_HH_MM_SLASH = "MM/dd/yyyy HH:mm";
        public static let MM_DD_YYYY_SLASH = "MM/dd/yyyy";
        public static let MM_DD_YYYY_HH_MM_A_SLASH = "MM/dd/yyyy hh:mm a";
        public static let MM_DD_YYYY_HH_MM_A_Z_SLASH = "MM/dd/yyyy hh:mm a z";
        public static let MMM_D_YYYY = "MMM d, yyyy";
        public static let MMM_D_HH_MM_A = "MMM d, hh:mm a";
        public static let MMM_DD = "MMM dd";
        public static let MMM_DD_HH_MM_A = "MMM dd, hh:mm a";
        public static let MMM_DD_HH_MM_A_DELIMITED = "MMM dd, - hh:mm a";
        public static let MMM_DD_YYYY = "MMM dd, yyyy";
        public static let MMM_DD_YYYY_HH_MM = "MMM dd, yyyy HH:mm";
        public static let MMM_DD_YYYY_HH_MM_A = "MMM dd, yyyy hh:mm a";
        public static let MMM_DD_YYYY_HH_MM_A_DELIMITED = "MMM dd, yyyy - hh:mm a";
        public static let MMM_DD_YYYY_HH_MM_SS_A = "MMM dd, yyyy - hh:mm:ss a";
        public static let MMM_DD_YYYY_HH_MM_A_DELIMITED_ZZZ = "MMM dd, yyyy - HH:mm: a zzz";
        public static let MMMM_YYYY = "MMMM yyyy";
        public static let MMMM_DD = "MMMM dd";
        public static let MMMM_DD_YYYY = "MMMM dd, yyyy";
        public static let YYYY = "yyyy";
        public static let YYYY_MM_DD = "yyyy-MM-dd";
        public static let YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
        public static let YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss"; 
        public static let YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
        public static let YYYY_MM_DD_T_HH_MM_SS_Z = "yyyy-MM-dd'T'HH:mm:ss'Z'";
        public static let YYYY_MM_DD_T_HH_MM_SS_SSS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        public static let C_SHARP_TIME_FORMAT = "'PT'HH'H'mm'M'ss.S'S'";
        public static let C_SHARP_TIME_FORMAT_HOUR = "'PT'HH'H'";
        public static let C_SHARP_TIME_FORMAT_HOUR_MINUTE = "'PT'HH'H'mm'M'";
    }
    
    struct CaseListFilter {
        static let cfCase        = "caseFilterCase"
        static let cfFromDate    = "caseFilterFromDate"
        static let cfToDate      = "caseFilterToDate"
        static let cfDSN         = "caseFilterDSN"
        static let cfType        = "caseFilterType"
        static let cfZip         = "caseFilterZip"
        static let cfSortBy      = "caseFilterSortBy"
        static let isCaseFilterApplied      = "isCaseFilterApplied"
        
        static let kSortCaseNumber   = "caseNumber"
        static let kSortIncidentDate = "incidentDate"
    }
    
    struct NotificationKeys {
        static let fileDownloadStatusKey        = "dStatus-%@"
        static let fileUploadStatusKey          = "uStatus-%@"
    }
    
    struct AlertsFilter {
        static let afFromDate   = "alertFilterFromDate"
        static let afToDate     = "alertFilterToDate"
        static let afAlertType  = "alertFilterAlertType"
        static let isAlertsFilterApplied = "isAlertsFilterApplied"
    }
    
    struct LoginStatus {
        static let keyLogin             = "ETWLogin"
        static let valueUserLoggedIn    = "ETWUserLoggedIn"
        static let valueUserLoggedOut   = "ETWUserLoggedOut"
    }
    
    struct EvidenceTransferStatus {
        static let initiated = "I"
        static let cancel = "C"
        static let reject = "R"
        static let accept = "A"
        static let finalStatus = "F"
    }
    
    struct UsePrevious {
        static let usePreviousStreet = "usePreviousStreet"
        static let usePreviousApt = "usePreviousApt"
        static let usePreviousCity = "usePreviousCity"
        static let usePreviousZip = "usePreviousZip"
        static let usePreviousPhoneNumber = "usePreviousPhoneNumber"
    }
    
    struct MasterSync {
        static let masterSyncLastSyncDate   = "masterSyncLastSyncDate"
        static let masterSyncStatus         = "masterSyncStatus"
        static let masterSyncCompleted      = "masterSyncCompleted"
        static let masterSyncNotSynced      = "masterSyncNotSynced"
    }
    
    struct IDScanSDK {
        static let iOSCameraScanningSDKKey = "iOSCameraScanningSDKKey"
        static let DriverLicenseParserCurrentSerial = "DriverLicenseParserCurrentSerial"
    }
    
    struct FirebaseCloudMessaging {
        static let fcmRegistrationToken = "fcmRegistrationToken"
        static let isFCMTokenRegisteredOnServer = "isFCMTokenRegisteredOnServer"
        static let openAlertsScreenOnPushNotificationSelection = "OpenAlertsScreenOnPushNotificationSelection"
    }
}
extension Dictionary {
    func toJson() {
        let jsonData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print("JSON string = \(jsonString)")
    }
}
extension UIView {
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
}
