import UIKit
import AVFoundation

class BarCodeScanner : NSObject {
    // BarCodes Strings array or error
    typealias ScanCompletionBlock =  (NSError?,String) -> Void
    
    private var captureDevice       : AVCaptureDevice?
    private var captureSession      : AVCaptureSession?
    private var captureDeviceInput  : AVCaptureDeviceInput?
    private var captureMetaOutput   : AVCaptureMetadataOutput?
    private var capturePreviewLayer : AVCaptureVideoPreviewLayer?
    
    private var torchMode           : AVCaptureDevice.TorchMode     = .off
    
    // Completion block for scanning
    private var sCompletionBlock    : ScanCompletionBlock?
    // Camera preview layer rendered on the preview view
    weak var previewView            : UIView?
    
    var rectOfInterest              : CGRect? {
        didSet{
            self.updateRectOfInterest()
        }
    }
    
    init(previewView : UIView) {
        self.previewView = previewView
    }
    
    var metaOptions : [AVMetadataObject.ObjectType] = [.qr,.upce,.code39,.code39Mod43,.ean8,.ean13,.code93,.code128,.pdf417,.aztec,.itf14,.dataMatrix,.interleaved2of5]
    
    // MARK: Public
    /// Start scanning the codes.
    func startScan(_ completion:@escaping ScanCompletionBlock) throws {
        if self.isScanning() {
            return
        }
        self.sCompletionBlock = completion
        self.addObservers()
        try self.config()
        self.captureSession?.startRunning()
    }
    
    func stopScan() throws {
        if self.captureSession == nil {
            return
        }
        
        self.torchMode = .off
        try updateTorch()
        self.capturePreviewLayer?.removeFromSuperlayer()
        self.capturePreviewLayer = nil
        self.captureSession?.stopRunning()
        self.captureSession = nil
        self.captureDevice = nil
        self.captureMetaOutput = nil
        self.captureDeviceInput = nil
        self.sCompletionBlock = nil
        NotificationCenter.default.removeObserver(self)
    }
    
    func toggleTorch() throws {
        if let camera = self.captureDevice, camera.hasTorch {
            self.torchMode = (self.torchMode == .off) ? .on : .off
            try self.updateTorch()
        }
    }
    
    
    func isTorchOn() -> Bool {
        return (self.torchMode == .on)
    }
    
    func hasTorch() -> Bool {
        return self.captureDevice?.hasTorch ?? false
    }

    // MARK: Private
    /// Capture device is nil if status is AVAuthorizationStatusRestricted
    /// - Returns: Bool
    private func updateTorch() throws {
        if let camera = self.captureDevice, camera.hasTorch {
            try camera.lockForConfiguration()
            camera.torchMode = self.torchMode
            camera.unlockForConfiguration()
        }
    }

    private func notifyScanCodeCompletion(error: NSError?,barCode:String) {
        if let block = self.sCompletionBlock {
            DispatchQueue.main.async {
                block(error, barCode)
            }
        }
    }

    func isScanning() -> Bool{
        return (self.captureSession != nil)
    }
    
    private func config() throws {
        self.captureDevice = try self.getCaptureDevice()
        self.captureSession = self.getCaptureSession()
        self.captureSession?.beginConfiguration()
        // Setup Input Output configuration for the device
        if let device = self.captureDevice {
            try self.configInput(device: device)
            try self.configOutput(device: device)
            self.configPreviewLayer()
            self.refreshOrientation()
        }
        else {
            //TODO : Log Error
            // Should not be here
        }
        self.captureSession?.commitConfiguration()
    }
    
    private func configInput(device:AVCaptureDevice) throws{
        if let input =  try self.getCaptureDeviceInput(device: device) {
            self.captureDeviceInput = input
            self.captureSession?.addInput(input)
        }
    }
    
    private func configOutput(device:AVCaptureDevice) throws {
        if let output  =  try self.getCaptureOutput(device: device) {
            self.captureMetaOutput = output
            self.captureSession?.addOutput(output)
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            output.metadataObjectTypes = self.metaOptions
        }
    }
    
    private func updateRectOfInterest() {
        if !self.isScanning() {
            return;
        }
        self.refreshOrientation()
        self.captureMetaOutput?.rectOfInterest = self.rectOfInterestForScanning(scanRect: self.rectOfInterest)
    }
    
    private func configPreviewLayer() {
        self.capturePreviewLayer = self.getPreviewLayer()
        self.captureMetaOutput?.rectOfInterest = self.rectOfInterestForScanning(scanRect: self.rectOfInterest)
    }
    
    private func getPreviewLayer() -> AVCaptureVideoPreviewLayer? {
        var previewLayer : AVCaptureVideoPreviewLayer?
        if let session = self.captureSession, let preview = self.previewView {
            previewLayer = AVCaptureVideoPreviewLayer(session: session)
            previewLayer?.videoGravity = .resizeAspectFill
            if let layer = previewLayer {
                DispatchQueue.main.async {
                    layer.frame = preview.bounds
                    preview.layer.addSublayer(layer)
                }
            }
        }
        return previewLayer
    }
    
    private func getCaptureDevice() throws -> AVCaptureDevice? {
        var device = self.captureDevice
        if device == nil {
            if #available(iOS 10.0, *) {
                device = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
            } else {
                // Fallback on earlier versions
                let devices = AVCaptureDevice.devices(for: .video)
                for d in devices {
                    if d.position == .back {
                        device = d
                    }
                }
            }
            // Setting default autofocus enabled for the device
            // Some devices does not support autofocus. Check the availability
            if let captureDevice = device{
                try self.configAutoFocusPreferences(captureDevice)
            }
        }
        return device
    }
    
    private func configAutoFocusPreferences(_ captureDevice: AVCaptureDevice) throws {
        try captureDevice.lockForConfiguration()
        if captureDevice.isFocusModeSupported(.continuousAutoFocus) {
            if captureDevice.isAutoFocusRangeRestrictionSupported {
                captureDevice.autoFocusRangeRestriction = .near
            }
            
            if captureDevice.isFocusPointOfInterestSupported {
                captureDevice.focusPointOfInterest = CGPoint(x: 0.5, y: 0.5) //
            }
            captureDevice.focusMode = .continuousAutoFocus
        }
        else if captureDevice.isFocusModeSupported(.autoFocus) {
            captureDevice.focusMode = .autoFocus
        }
        captureDevice.unlockForConfiguration()
    }
    
    private func getCaptureDeviceInput(device:AVCaptureDevice) throws -> AVCaptureDeviceInput? {
        var inputDevice = self.captureDeviceInput
        if inputDevice == nil {
            inputDevice = try AVCaptureDeviceInput(device: device)
        }
        return inputDevice
    }
    
    private func getCaptureOutput(device: AVCaptureDevice) throws -> AVCaptureMetadataOutput? {
        var output = self.captureMetaOutput
        if output == nil {
            output = AVCaptureMetadataOutput()
        }
        return output
    }
    
    private func getCaptureSession() -> AVCaptureSession? {
        var session = self.captureSession
        if session == nil {
            session = AVCaptureSession()
        }
        return session
    }
    
    private func rectOfInterestForScanning(scanRect: CGRect?) -> CGRect {
        if let scanRect = scanRect, !scanRect.isEmpty{
            return scanRect
        }
        return CGRect(x: 0, y: 0, width: 1, height: 1) // Default RectOfInterest for Scanning
    }
    
    private func refreshOrientation() {
        let currentOrientation = UIApplication.shared.statusBarOrientation
        if let previewView = self.previewView {
            self.capturePreviewLayer?.frame = previewView.bounds;
            if let isVideoOrientationSupported = capturePreviewLayer?.connection?.isVideoOrientationSupported, isVideoOrientationSupported == true {
                self.capturePreviewLayer?.connection?.videoOrientation = self.captureOrientationFrom(interfaceOrientation: currentOrientation)
            }
        }
    }
    
    private func captureOrientationFrom(interfaceOrientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch interfaceOrientation {
        case.portrait :
            return AVCaptureVideoOrientation.portrait
        case .landscapeLeft:
            return AVCaptureVideoOrientation.landscapeLeft
        case .landscapeRight:
            return AVCaptureVideoOrientation.landscapeRight
        case .portraitUpsideDown:
            return AVCaptureVideoOrientation.portraitUpsideDown
        default:
            return AVCaptureVideoOrientation.portrait
        }
    }
     
    // MARK: Observers
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(avCaptureInputPortFormatDescriptionDidChangeNotification(_:)), name:NSNotification.Name.AVCaptureInputPortFormatDescriptionDidChange, object: nil)
//        // Observer for orientation change
        NotificationCenter.default.addObserver(self, selector: #selector(didChangeOrientation(_:)), name: UIApplication.didChangeStatusBarOrientationNotification, object: nil)
    }
    
    @objc func didChangeOrientation(_ notification: NSNotification)  {
        self.refreshOrientation()
    }
    
    @objc func avCaptureInputPortFormatDescriptionDidChangeNotification(_ notification: NSNotification) {
        captureMetaOutput?.rectOfInterest = self.rectOfInterestForScanning(scanRect: self.rectOfInterest)
    }
    
    // NOTE: SomeTimes Scanned barcode returns wrong VIN number or extra 'I' leading character appended to the code. In those cases we need to trim down the barcode to vin number.
    // Referenced answer : https://github.com/mikebuss/MTBBarcodeScanner/issues/27
    private func extractVIN(code: AVMetadataMachineReadableCodeObject) -> String? {
        var vin: String?
        if let stringValue = code.stringValue {
            if code.type.rawValue == "org.iso.Code39" {
                if stringValue.count == 18 {
                    vin = String(stringValue.suffix(17))
                } else if stringValue.count == 19 {
                    vin = String(stringValue.suffix(18).prefix(17))
                } else if stringValue.count == 20 {
                    vin = String(stringValue.suffix(18).prefix(17))
                } else {
                    vin = stringValue
                }
                return vin
            }
            vin = stringValue
        }
        
        return vin
    }
}


extension BarCodeScanner : AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        var barCodes : [String] = []
        var error : NSError?
        var barCode = ""
        for dataObject in metadataObjects {
            if let code = self.capturePreviewLayer?.transformedMetadataObject(for: dataObject) as? AVMetadataMachineReadableCodeObject, let value = self.extractVIN(code: code) {
                barCodes.append(value)
            }
        }
        if barCodes.count == 0 {
            error = NSError(domain: "BarCodeScanner", code: 100, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, No BarCode found"])
        }
        else {
            barCode = barCodes.first!
        }
        self.notifyScanCodeCompletion(error: error, barCode: barCode)
    }
}
