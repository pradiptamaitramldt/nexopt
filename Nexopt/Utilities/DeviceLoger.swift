//
//  DeviceLoger.swift
//  VoiceCore
//
//  Created by user on 12/19/18.
//  Copyright © 2018. All rights reserved.
//

import Foundation
import MessageUI

@objc enum LoggerType: Int {
    case Info, NetworkOperation, Error, Exception
}

public class DeviceLoger: NSObject {
    /*
     LogLevel 0 : No Logs
     LogLevel 1 : Log only Exception
     LogLevel 2 : Log Exception, Error, NetworkError
     LogLevel 3 : Log Exception, Error, NetworkError, Network Activity
     LogLevel 4 : Log Exception, Error, NetworkError, Network Activity, Information
     */
    static let CurrentLogLevel = 4 //Range: 0 to 4
    
    static let infoLogFileCurrent = "currentInfoLogFile.txt"
    static let infoLogFileOld = "oldInfoLogFile.txt"
    
    static let errorLogFileCurrent = "currentErrorLogFile.txt"
    static let errorLogFileOld = "oldLogErrorFile.txt"
    
    static let fileSizeLimitBytes = (1024 * 1024 * 2) /*2MB*/
    static var __currenInfoLogFileHandle: FileHandle? = nil
    static var __currentErrorLogFileHandle: FileHandle? = nil

    //MARK: Setup
    class func makeReady() {
        if __currenInfoLogFileHandle == nil {
            //trim
            self.trimLogFileForMinAllowedSize(currentFilePath: self.filePathForFilename(infoLogFileCurrent), oldFilePath: self.filePathForFilename(infoLogFileOld), fileHandle: nil)
            //Create file handle
            self.createInfoFileHandle()
        }
        
        if __currentErrorLogFileHandle == nil {
            self.trimLogFileForMinAllowedSize(currentFilePath: self.filePathForFilename(errorLogFileCurrent), oldFilePath: self.filePathForFilename(errorLogFileOld), fileHandle: nil)
            
            self.createErrorFileHandle()
        }
    }
    
    class func cleanupInfoFileHandle() {
        __currenInfoLogFileHandle?.closeFile()
        __currenInfoLogFileHandle = nil
    }
    
    class func cleanupErrorFileHandle() {
        __currentErrorLogFileHandle?.closeFile()
        __currentErrorLogFileHandle = nil
    }
    
    class func createInfoFileHandle() {
        if __currenInfoLogFileHandle == nil {
            let path: String = self.filePathForFilename(infoLogFileCurrent)
            if !FileManager.default.fileExists(atPath: path) {
                FileManager.default.createFile(atPath: path, contents: nil, attributes: nil)
                self.appendDataToFile(filePath: path, data: "[".data(using: String.Encoding.utf8)!)
            }
            __currenInfoLogFileHandle = FileHandle(forUpdatingAtPath: path)
            __currenInfoLogFileHandle?.seekToEndOfFile()
        }
    }
    
    class func createErrorFileHandle() {
        if __currentErrorLogFileHandle == nil {
            let path: String = self.filePathForFilename(errorLogFileCurrent)
            if !FileManager.default.fileExists(atPath: path) {
                FileManager.default.createFile(atPath: path, contents: nil, attributes: nil)
                self.appendDataToFile(filePath: path, data: "[".data(using: String.Encoding.utf8)!)
            }
            __currentErrorLogFileHandle = FileHandle(forUpdatingAtPath: path)
            __currentErrorLogFileHandle?.seekToEndOfFile()
        }
    }
    
    class func filePathForFilename(_ filename: String) -> String {
        let paths: [Any] = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        let cacheDir = paths[0] as! String
        let createPath  = cacheDir.appending("/\(filename)")
        return createPath
    }
    
    class func trimLogFileForMinAllowedSize(currentFilePath: String, oldFilePath: String, fileHandle: FileHandle?) {
        do {
            //  try FileManager.default.attributesOfItem(atPath: currentFilePath)[FileAttributeKey.size]
            let fileSize = try FileManager.default.attributesOfItem(atPath: currentFilePath)[FileAttributeKey.size]
            
            //if current file size if grater then allowed size limit
            if (fileSize  as? UInt64)! > UInt64(fileSizeLimitBytes) {
                //if old file exist remove it
                if FileManager.default.fileExists(atPath: oldFilePath) {
                    try? FileManager.default.removeItem(atPath: oldFilePath)
                }
                
                //rename current file to old file
                try? FileManager.default.moveItem(atPath: currentFilePath, toPath: oldFilePath)
                
                if fileHandle == __currenInfoLogFileHandle {
                    self.cleanupInfoFileHandle()
                } else if fileHandle == __currentErrorLogFileHandle {
                    self.cleanupErrorFileHandle()
                }
            }
            
        } catch{
            print("Error moving or checking file for existance - \(error.localizedDescription)")
        }
    }
    
    //MARK: Logging
    @objc static func log(_ message: String, type: LoggerType) {
        switch type {
        case .Info:
            DeviceLoger.logInfo(message)
        case .Error:
            DeviceLoger.logError(error: message)
        case .Exception:
            DeviceLoger.logException(exception: NSException.init(name: NSExceptionName.init("Exception"), reason: nil, userInfo: nil), withMessage: message)
        case .NetworkOperation:
            DeviceLoger.logInfo("NTW:" + message)
        }
    }
    
    @objc class func logError(error: String) {
        if CurrentLogLevel > 1 {
            self.saveToErrorFile(log: self.getJSONStringForMessage(message: error, type: LoggerType.Error, callStack: Thread.callStackSymbols))
        }
    }
    
    @objc class func logException(exception: NSException) {
        if CurrentLogLevel > 0 {
            self.saveToErrorFile(log: self.getJSONStringForMessage(message: exception.name.rawValue, type: LoggerType.Exception, callStack: exception.callStackSymbols))
        }
    }
    
    @objc class func logException(exception: NSException, withMessage message: String) {
        if CurrentLogLevel > 0 {
            let excpWithMsg = "\(exception.name) :: \(message)"
            self.saveToErrorFile(log: self.getJSONStringForMessage(message: excpWithMsg, type: LoggerType.Exception, callStack: exception.callStackSymbols))
        }
    }
    
    @objc class func logNetworkRequestWithUrl(url: String, httpMethod: String, contentLength: String, otherParameter other: String) {
        if CurrentLogLevel > 2 {
            var jsonString: String
            let typeString: String = "NTWK"
            let date: String = LoggerToolKit.getCurrentDateRFC3339Format()
            let logDictionary: [AnyHashable: Any] = ["date": date, "type": typeString, "requestURL": url, "httpMethod": httpMethod, "contentLength": contentLength, "other": other]
            
            jsonString = LoggerToolKit.getJSONStringForDictionary(convert: logDictionary)
            self.saveToInfoFile(log: jsonString)
        }
    }
    
    @objc class func logNetworkResponse(withUrl responseCode: String, contentType: String, contentLength: String) {
        if CurrentLogLevel > 2 {
            var jsonString: String
            let typeString: String = "NTWK"
            let date: String = LoggerToolKit.getCurrentDateRFC3339Format()
            let logDictionary: [AnyHashable: Any] = ["date": date, "type": typeString, "responseCode": responseCode, "contentLength": contentLength]
            jsonString = LoggerToolKit.getJSONStringForDictionary(convert: logDictionary)
            self.saveToInfoFile(log:jsonString)
        }
    }
    
    @objc class func logNetworkError(withUrl url: String, requestMethod: String, requestContentLength: String, responseCode: String, contentType: String, responseContentLength: String) {
        if CurrentLogLevel > 1 {
            var jsonString: String
            let typeString: String = "NTWK"
            let date: String = LoggerToolKit.getCurrentDateRFC3339Format()
            let logDictionary: [AnyHashable: Any] = ["date": date, "type": typeString, "requestURL": url, "requestMethod": requestMethod, "requestContentLength": requestContentLength, "responseCode": responseCode, "contentType": contentType, "responseContentLength": responseContentLength]
            jsonString = LoggerToolKit.getJSONStringForDictionary(convert: logDictionary)
            self.saveToInfoFile(log: jsonString)
        }
    }
    
    @objc class func logInfo(_ infoMsg: String) {
        if CurrentLogLevel > 3 {
            //TODO: make provision to deal with "How/Should not to pass empty array to call stack"
            let emptyCallStack = [Any] ()
            self.saveToInfoFile(log: self.getJSONStringForMessage(message: infoMsg, type: LoggerType.Info, callStack: emptyCallStack))
        }
    }
    
    @objc class func sendErrorLogFile(toMail fromController: UIViewController)  {
        self.logInfo("Starting logging Info")
        self.logError(error: "Starting logging Error")
        //self.logException(exception: nil, withMessage: "Starting logging Exception")
        
        
        if MFMailComposeViewController.canSendMail() {
            let mailController = MFMailComposeViewController()
            mailController.mailComposeDelegate = fromController as? MFMailComposeViewControllerDelegate
            mailController.setSubject("Log files")
            
            var doesContainFiles = false
            let allFilenames = [errorLogFileCurrent, errorLogFileOld, infoLogFileCurrent, infoLogFileOld]
            for filename in allFilenames {
                let filePath = self.filePathForFilename(filename)
                
                if FileManager.default.fileExists(atPath: filePath) {
                    let fileURL = URL(fileURLWithPath: filePath)
                    if let data = NSData(contentsOf: fileURL){
                        mailController.addAttachmentData(data as Data, mimeType: "text/plain", fileName: filename)
                        doesContainFiles = doesContainFiles || true
                    }
                }
            }
            
            if doesContainFiles {
                fromController.present(mailController, animated: true, completion: nil)
            } else {
                let window = UIWindow(frame: UIScreen.main.bounds)
                window.rootViewController = UIViewController()
                window.windowLevel = UIWindow.Level.alert + 1
                let alertController = UIAlertController(title: "Info", message: "Log file does not exist", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                window.makeKeyAndVisible()
                window.rootViewController?.present(alertController , animated: true, completion: {})
            }
        }
        else {
            let alertController = UIAlertController(title: "Logger", message: "Mail Configuration Required", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            fromController.present(alertController, animated: false, completion: nil)
            print("Mail Configuration Required")
        }
    }
    
    class  func appendDataToFile(filePath: String, data: Data) {
        let handler = FileHandle(forWritingAtPath: filePath)
        handler?.seekToEndOfFile()
        handler?.write(data)
        handler?.closeFile()
    }
    
    class func saveToInfoFile(log: String) {
        self.trimLogFileForMinAllowedSize(currentFilePath: self.filePathForFilename(infoLogFileCurrent), oldFilePath: infoLogFileOld, fileHandle: __currenInfoLogFileHandle)
        
        self.createInfoFileHandle()
        
        let lockQueue = DispatchQueue(label: "__currentInfoFileHandle")
        lockQueue.sync {
            var logdata: Data? = log.data(using: String.Encoding.utf8)
            logdata?.append(",".data(using: String.Encoding.utf8)!)
            __currenInfoLogFileHandle?.write(logdata!)
        }
    }
    
    class func saveToErrorFile(log: String) {
        self.trimLogFileForMinAllowedSize(currentFilePath: self.filePathForFilename(infoLogFileCurrent), oldFilePath: infoLogFileOld, fileHandle: __currentErrorLogFileHandle)
        
        self.createErrorFileHandle()
        
        let lockQueue = DispatchQueue(label: "__currentErrorFileHandle")
        lockQueue.sync {
            var logdata: Data? = log.data(using: String.Encoding.utf8)
            logdata?.append(",".data(using: String.Encoding.utf8)!)
            __currentErrorLogFileHandle?.write(logdata!)
        }
    }
    
    class func getJSONStringForMessage( message: String, type: LoggerType, callStack: [Any]) -> String {
        var jsonString: String
        var typeString: String
        let date: String = LoggerToolKit.getCurrentDateRFC3339Format()
        var logDictionary: [AnyHashable: Any] = ["date": date, "message": message]
        switch type {
        case LoggerType.Exception:
            typeString = "EXCP"
            logDictionary["callStack"] = callStack
        case LoggerType.Error:
            typeString = "ERRR"
            logDictionary["callStack"] = callStack
        case LoggerType.NetworkOperation:
            typeString = "NTWK"
        case LoggerType.Info:
            typeString = "INFO"
        default:
            typeString = "UNKWN"
        }
        
        logDictionary["type"] = typeString
        
        jsonString = LoggerToolKit.getJSONStringForDictionary(convert: logDictionary)
        return jsonString
    }
}

class LoggerToolKit: NSObject {
    
    class func getCurrentDateRFC3339Format() -> String {
        let now = Date()
        let localTimeZone = NSTimeZone.system
        let rfc3339DateFormatter = DateFormatter()
        let enUSPOSIXLocale = NSLocale(localeIdentifier: "en_US_POSIX")
        rfc3339DateFormatter.locale = enUSPOSIXLocale as Locale
        rfc3339DateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZ"
        rfc3339DateFormatter.timeZone = localTimeZone as TimeZone
        let dateString: String = rfc3339DateFormatter.string(from: now)
        //NSLog(@"RFC 3339 datetime: %@", dateString); //Format : 2017-08-23T18:58:21+0530
        return dateString
    }
    
    class func getJSONStringForDictionary (convert: [AnyHashable: Any]) -> String {
        var jsonString: String = ""
        //let error: Error?
        do {
            let data = try JSONSerialization.data(withJSONObject:convert, options:[])
            jsonString = String(data: data, encoding: String.Encoding.utf8)!
            //print(jsonString)
        } catch {
            print("JSON serialization failed:  \(error)")
        }
        return jsonString
    }
}
