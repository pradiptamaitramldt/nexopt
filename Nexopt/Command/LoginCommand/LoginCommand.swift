//
//  LoginCommand.swift
//  Nexopt
//
//  Created by Pradipta on 25/11/19.
//  Copyright © 2019 Brainium. All rights reserved.
//

import UIKit

class LoginCommand: BaseCommand {
    var userID : String?
    var password : String?
    
    init(userID: String?, password: String?) {
        self.userID = userID
        self.password = password
    }
    
    override func validate(completion: @escaping(Bool, String?) -> Void) {
        
        if(self.userID == nil || self.userID?.count == 0){
            completion(false, "Please enter user id")
        }else if(self.isValidEmail(emailStr: self.userID)==false){
            completion(false, "Please enter valid user id")
        }else if(self.password == nil || self.password?.count == 0){
            completion(false, "Please enter password")
        }else{
            completion(true, "")
        }
    }
    
    func isValidEmail(emailStr:String?) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
}
